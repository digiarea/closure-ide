/*******************************************************************************
 * Licensed Materials - Property of DigiArea, Inc.
 * � Copyright DigiArea Corporation 2014. All Rights Reserved.
 *******************************************************************************/

package com.digiarea.gct.core;

/**
 * @author daginno
 * 
 */
public enum TypeKind {

	TYPE, NAMESPACE, OBJECT

}
