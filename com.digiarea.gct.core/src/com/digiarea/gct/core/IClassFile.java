/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.core;


/**
 * Represents an entire non-editable JavaScript file.
 * non-editable JavaScript file elements need to be opened before they can be navigated.
 * If a  file cannot be parsed, its structure remains unknown. Use
 * <code>IJavaScriptElement.isStructureKnown</code> to determine whether this is the
 * case.
 * <p>
 * This interface is not intended to be implemented by clients.
 * </p>
 *  
 * Provisional API: This class/interface is part of an interim API that is still under development and expected to 
 * change significantly before reaching stability. It is being made available at this early stage to solicit feedback 
 * from pioneering adopters on the understanding that any code that uses this API will almost certainly be broken 
 * (repeatedly) as the API evolves.
*/

public interface IClassFile extends ITypeRoot {

/**
 * Returns the bytes contained in this  file.
 *
 * @return the bytes contained in this  file
 *
 * @exception JavaScriptModelException if this element does not exist or if an
 *      exception occurs while accessing its corresponding resource
 */
byte[] getBytes() throws JavaScriptModelException;

/**
 * Returns the first type contained in this  file.
 * This is a handle-only method. The type may or may not exist.
 *
 * @return the type contained in this file
 *
 */
IType getType();
public IType[] getTypes() throws JavaScriptModelException ;


/* 
 * Returns whether this type is edit. This is not guaranteed to be
 * instantaneous, as it may require parsing the underlying file.
 *
 * @return <code>true</code> if the  file represents a class.
 *
 * @exception JavaScriptModelException if this element does not exist or if an
 *      exception occurs while accessing its corresponding resource
 */
boolean isClass() throws JavaScriptModelException;

/**
 * Returns the first import declaration in this javaScript file with the given name.
 * This is a handle-only method. The import declaration may or may not exist. This
 * is a convenience method - imports can also be accessed from a javaScript file's
 * import container.
 *
 *
 * <p><b>Note: This Method only applies to ECMAScript 4 which is not yet supported</b></p>
 *
 * @param name the name of the import to find
 * @return a handle onto the corresponding import declaration. The import declaration may or may not exist.
 */
IRequireDeclaration getRequire(String name) ;

IProvideDeclaration getProvide(String name) ;
/**
 * Returns the import container for this javaScript file.
 * This is a handle-only method. The import container may or
 * may not exist. The import container can used to access the
 * imports.
 *
 * <p><b>Note: This Method only applies to ECMAScript 4 which is not yet supported</b></p>
 *
 * @return a handle onto the corresponding import container. The
 *		import contain may or may not exist.
 */
IRequireContainer getRequireContainer();

IProvideContainer getProvideContainer();
/**
 * Returns the import declarations in this javaScript file
 * in the order in which they appear in the source. This is
 * a convenience method - import declarations can also be
 * accessed from a javaScript file's import container.
 *
 * <p><b>Note: This Method only applies to ECMAScript 4 which is not yet supported</b></p>
 *
 * @return the import declarations in this javaScript file
 * @throws JavaScriptModelException if this element does not exist or if an
 *		exception occurs while accessing its corresponding resource
 */
IRequireDeclaration[] getRequires() throws JavaScriptModelException;

IProvideDeclaration[] getProvides() throws JavaScriptModelException;

}
