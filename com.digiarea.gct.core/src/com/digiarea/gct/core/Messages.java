/*******************************************************************************
 * Copyright (c) 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.core;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.osgi.util.NLS;

import com.ibm.icu.text.MessageFormat;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "com.digiarea.gct.core.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	public static String LibWriter_LibWriteError;

	public static String LibWriter_BuildpathWriteError;

	public static String LibWriter_notice;

	public static String LibWriter_DepsWriteError;

	public static String LibWriter_LibWrite;

	public static String LibWriter_DepsCreateError;

	private Messages() {
	}

	static {
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	public static String getString(String key, Object[] objects) {
		try {
			return MessageFormat.format(RESOURCE_BUNDLE.getString(key), objects);
		}
		catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
