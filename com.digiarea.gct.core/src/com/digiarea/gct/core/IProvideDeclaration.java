package com.digiarea.gct.core;

/**
 * Represents an provide declaration in JavaScript unit.
 * <p>
 * This interface is not intended to be implemented by clients.
 * </p>
 *  
 * Provisional API: This class/interface is part of an interim API that is still under development and expected to 
 * change significantly before reaching stability. It is being made available at this early stage to solicit feedback 
 * from pioneering adopters on the understanding that any code that uses this API will almost certainly be broken 
 * (repeatedly) as the API evolves.
 */
public interface IProvideDeclaration extends IJavaScriptElement, ISourceReference, ISourceManipulation {
/**
 * Returns the name that has been provided.
 *
 * @return the name that has been provided
 */
String getElementName();

/**
 * Returns whether the provide is implicit. A provide is implicit if it declared by <code>goog.provide</code> expression.
 * @return true if the provide is implicit, false otherwise
 */
boolean isImplicit();
}
