/*******************************************************************************
 * Licensed Materials - Property of DigiArea, Inc.
 * � Copyright DigiArea Corporation 2014. All Rights Reserved.
 *******************************************************************************/

package com.digiarea.gct.core.infer;

import com.digiarea.gct.core.compiler.CharOperation;

/**
 * @author daginno
 *
 */
public class InferEngineUtils {
	
	/**
	 * <p>
	 * Creates an anonymous type name from the given variable name.
	 * </p>
	 * 
	 * @param varName
	 *            to use when creating the anonymous type name
	 * @return
	 */
	public static char[] createAnonymousGlobalTypeName(char[] varName) {
		return CharOperation.concat(CharOperation.concat(IInferEngine.ANONYMOUS_PREFIX, IInferEngine.ANONYMOUS_CLASS_ID), varName, '_');
	}

}
