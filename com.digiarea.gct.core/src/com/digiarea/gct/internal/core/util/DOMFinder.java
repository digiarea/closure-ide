/*******************************************************************************
 * Copyright (c) 2005, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core.util;

import com.digiarea.gct.core.IInitializer;
import com.digiarea.gct.core.IMember;
import com.digiarea.gct.core.ISourceRange;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.ASTVisitor;
import com.digiarea.gct.core.dom.AnonymousClassDeclaration;
import com.digiarea.gct.core.dom.Assignment;
import com.digiarea.gct.core.dom.ClassInstanceCreation;
import com.digiarea.gct.core.dom.ExpressionStatement;
import com.digiarea.gct.core.dom.FunctionDeclaration;
import com.digiarea.gct.core.dom.FunctionExpression;
import com.digiarea.gct.core.dom.IBinding;
import com.digiarea.gct.core.dom.ImportDeclaration;
import com.digiarea.gct.core.dom.Initializer;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.PackageDeclaration;
import com.digiarea.gct.core.dom.TypeDeclaration;
import com.digiarea.gct.core.dom.VariableDeclarationFragment;
import com.digiarea.gct.internal.core.SourceRefElement;

public class DOMFinder extends ASTVisitor {

	public ASTNode foundNode = null;
	public IBinding foundBinding = null;

	private JavaScriptUnit ast;
	private SourceRefElement element;
	private boolean resolveBinding;
	private int rangeStart = -1, rangeLength = 0;

	public DOMFinder(JavaScriptUnit ast, SourceRefElement element, boolean resolveBinding) {
		this.ast = ast;
		this.element = element;
		this.resolveBinding = resolveBinding;
	}

	protected boolean found(ASTNode node, ASTNode name) {
		if (name != null && name.getStartPosition() == this.rangeStart && name.getLength() == this.rangeLength) {
			this.foundNode = node;
			return true;
		}
		return false;
	}

	public ASTNode search() throws JavaScriptModelException {
		ISourceRange range = null;
		if (this.element instanceof IMember && !(this.element instanceof IInitializer))
			range = ((IMember) this.element).getNameRange();
		else
			range = this.element.getSourceRange();
		this.rangeStart = range.getOffset();
		this.rangeLength = range.getLength();
		this.ast.accept(this);
		return this.foundNode;
	}

	public boolean visit(AnonymousClassDeclaration node) {
		ASTNode name;
		ASTNode parent = node.getParent();
		switch (parent.getNodeType()) {
			case ASTNode.CLASS_INSTANCE_CREATION:
				name = ((ClassInstanceCreation) parent).getType();
				break;
			default:
				return true;
		}
		if (found(node, name) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}

	public boolean visit(ImportDeclaration node) {
		if (found(node, node) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}

	public boolean visit(Initializer node) {
		// note that no binding exists for an Initializer
		found(node, node);
		return true;
	}

	public boolean visit(FunctionDeclaration node) {
		if (found(node, node.getName()) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}
	
	public boolean visit(Assignment node) {
		
		if((node.getLeftHandSide().getNodeType() == ASTNode.FIELD_ACCESS || node.getLeftHandSide().getNodeType() == ASTNode.SIMPLE_NAME) && node.getRightHandSide().getNodeType() == ASTNode.FUNCTION_EXPRESSION){
			if (found(node, node.getLeftHandSide())){
				if(node.getParent() instanceof ExpressionStatement){
					this.foundNode = node.getParent();
				}else{
					this.foundNode = node;
				}
				if(this.resolveBinding){
					this.foundBinding = ((FunctionExpression) node.getRightHandSide()).getMethod().resolveBinding();
				}
			}
		}
		return true;
	}

	public boolean visit(PackageDeclaration node) {
		if (found(node, node) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}

	public boolean visit(TypeDeclaration node) {
		if (found(node, node.getName()) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}

	public boolean visit(VariableDeclarationFragment node) {
		if (found(node, node.getName()) && this.resolveBinding)
			this.foundBinding = node.resolveBinding();
		return true;
	}
}
