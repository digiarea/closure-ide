/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

import org.eclipse.core.runtime.Assert;

import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IRequireDeclaration;
import com.digiarea.gct.core.JavaScriptModelException;

/**
 * Handle for an import declaration. Info object is a ImportDeclarationElementInfo.
 * @see IRequireDeclaration
 */

public class RequireDeclaration extends SourceRefElement implements IRequireDeclaration {

	protected String name;
	protected boolean isOnDemand;
	protected boolean isImplicit;

/**
 * Constructs an ImportDeclaration in the given import container
 * with the given name.
 */
protected RequireDeclaration(RequireContainer parent, String name, boolean isOnDemand, boolean isImplicit) {
	super(parent);
	this.name = name;
	this.isOnDemand = isOnDemand;
	this.isImplicit = isImplicit;
}
public boolean equals(Object o) {
	if (!(o instanceof RequireDeclaration)) return false;
	return super.equals(o);
}
public String getElementName() {
	if (this.isOnDemand)
		return this.name + ".*"; //$NON-NLS-1$
	return this.name;
}
public String getNameWithoutStar() {
	return this.name;
}
/**
 * @see IJavaScriptElement
 */
public int getElementType() {
	return REQUIRE_DECLARATION;
}
/**
 * @see com.digiarea.gct.core.IRequireDeclaration#getFlags()
 */
public int getFlags() throws JavaScriptModelException {
	RequireDeclarationElementInfo info = (RequireDeclarationElementInfo)getElementInfo();
	return info.getModifiers();
}
/**
 * @see JavaElement#getHandleMemento(StringBuffer)
 * For import declarations, the handle delimiter is associated to the import container already
 */
protected void getHandleMemento(StringBuffer buff) {
	((JavaElement)getParent()).getHandleMemento(buff);
	escapeMementoName(buff, getElementName());
	if (this.occurrenceCount > 1) {
		buff.append(JEM_COUNT);
		buff.append(this.occurrenceCount);
	}
}
/**
 * @see JavaElement#getHandleMemento()
 */
protected char getHandleMementoDelimiter() {
	// For import declarations, the handle delimiter is associated to the import container already
	Assert.isTrue(false, "Should not be called"); //$NON-NLS-1$
	return 0;
}
/*
 * @see JavaElement#getPrimaryElement(boolean)
 */
public IJavaScriptElement getPrimaryElement(boolean checkOwner) {
	CompilationUnit cu = (CompilationUnit)this.parent.getParent();
	if (checkOwner && cu.isPrimary()) return this;
	return cu.getRequire(getElementName());
}
/**
 * Returns true if the import is on-demand (ends with ".*")
 */
public boolean isOnDemand() {
	return this.isOnDemand;
}
/* (non-Javadoc)
 * @see com.digiarea.gct.core.IRequireDeclaration#isImplicit()
 */
public boolean isImplicit() {
	return isImplicit;
}
/**
 */
public String readableName() {

	return null;
}
/**
 * @private Debugging purposes
 */
protected void toStringInfo(int tab, StringBuffer buffer, Object info, boolean showResolvedInfo) {
	buffer.append(this.tabString(tab));
	buffer.append("require "); //$NON-NLS-1$
	toStringName(buffer);
	if (info == null) {
		buffer.append(" (not open)"); //$NON-NLS-1$
	}
}

}
