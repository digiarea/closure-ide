/*******************************************************************************
 * Copyright (c) 2000, 2011, 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.formatter.DefaultCodeFormatterConstants;
import com.digiarea.gct.internal.compiler.impl.CompilerOptions;

/**
 * JavaScriptCore eclipse preferences initializer. Initially done in
 * JavaScriptCore.initializeDefaultPreferences which was deprecated with new
 * eclipse preferences mechanism.
 */
public class JavaCorePreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		// If modified, also modify the method
		// JavaModelManager#getDefaultOptionsNoInitialization()
		// Get options names set
		HashSet optionNames = JavaModelManager.getJavaModelManager().optionNames;

		// Compiler settings
		Map defaultOptionsMap = new CompilerOptions().getMap(); // compiler
																// defaults

		// Override some compiler defaults
		defaultOptionsMap.put(ClosureCore.COMPILER_LOCAL_VARIABLE_ATTR, ClosureCore.GENERATE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CODEGEN_UNUSED_LOCAL, ClosureCore.PRESERVE);
		defaultOptionsMap.put(ClosureCore.COMPILER_TASK_TAGS, ClosureCore.DEFAULT_TASK_TAGS);
		defaultOptionsMap.put(ClosureCore.COMPILER_TASK_PRIORITIES, ClosureCore.DEFAULT_TASK_PRIORITIES);
		defaultOptionsMap.put(ClosureCore.COMPILER_TASK_CASE_SENSITIVE, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_DOC_COMMENT_SUPPORT, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_PB_FORBIDDEN_REFERENCE, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_SEMANTIC_VALIDATION, ClosureCore.DISABLED);

		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_RESOURCE_COPY_FILTER, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_INVALID_CLASSPATH, ClosureCore.ABORT);
		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_DUPLICATE_RESOURCE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_CLEAN_OUTPUT_FOLDER, ClosureCore.CLEAN);
		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_RECREATE_MODIFIED_CLASS_FILES_IN_OUTPUT_FOLDER, ClosureCore.IGNORE);

		// JavaScriptCore settings
		defaultOptionsMap.put(ClosureCore.CORE_JAVA_BUILD_ORDER, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.CORE_INCOMPLETE_CLASSPATH, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.CORE_CIRCULAR_CLASSPATH, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.CORE_INCOMPATIBLE_JDK_LEVEL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.CORE_ENABLE_CLASSPATH_EXCLUSION_PATTERNS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.CORE_ENABLE_CLASSPATH_MULTIPLE_OUTPUT_LOCATIONS, ClosureCore.ENABLED);

		// encoding setting comes from resource plug-in
		optionNames.add(ClosureCore.CORE_ENCODING);

		// Formatter settings
		Map codeFormatterOptionsMap = DefaultCodeFormatterConstants.getEclipseDefaultSettings(); // code
																									// formatter
																									// defaults
		for (Iterator iter = codeFormatterOptionsMap.entrySet().iterator(); iter.hasNext();) {
			Map.Entry entry = (Map.Entry) iter.next();
			String optionName = (String) entry.getKey();
			defaultOptionsMap.put(optionName, entry.getValue());
			optionNames.add(optionName);
		}

		// CodeAssist settings
		defaultOptionsMap.put(ClosureCore.CODEASSIST_VISIBILITY_CHECK, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_DEPRECATION_CHECK, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_IMPLICIT_QUALIFICATION, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_FIELD_PREFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_STATIC_FIELD_PREFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_LOCAL_PREFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_ARGUMENT_PREFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_FIELD_SUFFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_STATIC_FIELD_SUFFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_LOCAL_SUFFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_ARGUMENT_SUFFIXES, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.CODEASSIST_FORBIDDEN_REFERENCE_CHECK, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_DISCOURAGED_REFERENCE_CHECK, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_CAMEL_CASE_MATCH, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.CODEASSIST_SUGGEST_STATIC_IMPORTS, ClosureCore.ENABLED);

		/*
		 * START -------------------------------- Bug 203292 Type/Method/Filed
		 * resolution error configuration ---------------------
		 */
		/*
		 * Default ERROR for unresolved types/fields/methods
		 */
		defaultOptionsMap.put(ClosureCore.UNRESOLVED_TYPE_REFERENCE, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.UNRESOLVED_FIELD_REFERENCE, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.UNRESOLVED_METHOD_REFERENCE, ClosureCore.ERROR);
		/*
		 * END -------------------------------- Bug 203292 Type/Method/Filed
		 * resolution error configuration ---------------------
		 */
		/*
		 * START -------------------------------- Bug 197884 Loosly defined
		 * var (for statement) and optional semi-colon ---------------------
		 */
		defaultOptionsMap.put(ClosureCore.LOOSE_VAR_DECL, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.OPTIONAL_SEMICOLON, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_PB_DUPLICATE_LOCAL_VARIABLES, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_PB_UNINITIALIZED_LOCAL_VARIABLE, ClosureCore.WARNING);
		/*
		 * END -------------------------------- Bug 197884 Loosly defined var
		 * (for statement) and optional semi-colon ---------------------
		 */

		/*
		 * START -------------------------------- Bug 417465 - JavaScript
		 * Validation reports max 100 problems per .js file
		 * ---------------------
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_PB_MAX_PER_UNIT, String.valueOf(100));
		/*
		 * END -------------------------------- Bug 417465 - JavaScript
		 * Validation reports max 100 problems per .js file
		 * ---------------------
		 */

		// Time out for parameter names
		defaultOptionsMap.put(ClosureCore.TIMEOUT_FOR_PARAMETER_NAME_FROM_ATTACHED_JAVADOC, "50"); //$NON-NLS-1$

		/*
		 * CLOSURE COMPILER PRODUCTION
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_ENABLED, String.valueOf(false)); //$NON-NLS-1$

		/*
		 * CLOSURE COMPILER RENAMING
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_DEBUG, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_EXPORT, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_AFFINITY, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_DEVIRTUALIZE, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_UNRELATED, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_UNRELATED_SAMENAME, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_VARIABLE_SHADOW, String.valueOf(false)); //$NON-NLS-1$

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_FUNCTION_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_FUNCTION_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_FUNCTION_POLICE, ClosureCore.COMPILER_CLOSURE_FUNCTION_POLICE_V1); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_PROPERTY_POLICE, ClosureCore.COMPILER_CLOSURE_PROPERTY_POLICE_V1); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_VARIABLE_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_VARIABLE_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_RENAME_VARIABLE_POLICE, ClosureCore.COMPILER_CLOSURE_VARIABLE_POLICE_V1); //$NON-NLS-1$

		/*
		 * CLOSURE COMPILER RENAMING PRODUCTION
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_DEBUG, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_EXPORT, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_AFFINITY, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_DEVIRTUALIZE, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_UNRELATED, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_UNRELATED_SAMENAME, String.valueOf(false)); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_VARIABLE_SHADOW, String.valueOf(false)); //$NON-NLS-1$

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_FUNCTION_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_FUNCTION_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_FUNCTION_POLICE, ClosureCore.COMPILER_CLOSURE_FUNCTION_POLICE_V1); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_PROPERTY_POLICE, ClosureCore.COMPILER_CLOSURE_PROPERTY_POLICE_V1); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_VARIABLE_INPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_VARIABLE_OUTPUT, ""); //$NON-NLS-1$
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_RENAME_VARIABLE_POLICE, ClosureCore.COMPILER_CLOSURE_VARIABLE_POLICE_V1); //$NON-NLS-1$

		/*
		 * CLOSURE COMPILER COMPILE_TIME DEFINES
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_COMPILETIME_DEFINES_NAME, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_COMPILETIME_DEFINES_TYPE, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_COMPILETIME_DEFINES_VALUE, "");

		/*
		 * CLOSURE COMPILER COMPILE_TIME DEFINES
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_COMPILETIME_DEFINES_NAME, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_COMPILETIME_DEFINES_TYPE, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_COMPILETIME_DEFINES_VALUE, "");

		/*
		 * CLOSURE COMPILER EXTRA JSDOCS
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_EXTRA_JSDOCS, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_EXTRA_JSDOCS, "");

		/*
		 * CLOSURE COMPILER TRANSLATION
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_TRANSLATION_FILE, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_TRANSLATION_PROJECT, "");

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_TRANSLATION_FILE, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_TRANSLATION_PROJECT, "");

		/*
		 * CLOSURE COMPILER SOURCE MAP
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_SOURCEMAP_FORMAT, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_SOURCEMAP_FILE, "");

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_SOURCEMAP_FORMAT, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_SOURCEMAP_FILE, "");

		/*
		 * CLOSURE COMPILER EXPORTS
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_EXPORTS_GENERATE, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_EXPORTS_EXTERN, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_EXPORTS_EXTERN_FILE, "");

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_EXPORTS_GENERATE, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_EXPORTS_EXTERN, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_EXPORTS_EXTERN_FILE, "");

		/*
		 * CLOSURE COMPILER COMPLIANCE
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_INPUT_COMPLIANCE, ClosureCore.VERSION_CLOSURE_1_3);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OUTPUT_COMPLIANCE, ClosureCore.VERSION_CLOSURE_1_3);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_CHARSET_COMPLIANCE, "UTF-8");

		/*
		 * CLOSURE COMPILER OUTPUT
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OUTPUT_INPUTDELIMITER, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OUTPUT_PRETTYPRINT, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OUTPUT_SINGLEQUOTES, String.valueOf(false));

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OUTPUT_INPUTDELIMITER, "");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OUTPUT_PRETTYPRINT, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OUTPUT_SINGLEQUOTES, String.valueOf(false));

		/*
		 * CLOSURE COMPILER PRIMITIVES
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRIMITIVES_CLOSURE, String.valueOf(true));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRIMITIVES_JQUERY, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRIMITIVES_ANGULAR, String.valueOf(false));

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PRIMITIVES_CLOSURE, String.valueOf(true));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PRIMITIVES_JQUERY, String.valueOf(false));
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PRIMITIVES_ANGULAR, String.valueOf(false));

		/*
		 * CLOSURE COMPILER OPTIMIZATIONS
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_CHAIN_CALLS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_CHAIN_CALLS, ClosureCore.DISABLED);

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_ALIAS_EXTERNALS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_ALIAS_KEYWORDS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_ALIAS_STRINGS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_ASSUME_CLOSUREONLY, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_ASSUME_STRICTTHIS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COALESCE_VARS_NAMES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COLLAPSE_ANONYMOUSFUNCTIONS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COLLAPSE_OBKECTLITERALS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COLLAPSE_PROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COLLAPSE_PROPERTIESEXTERNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_COLLAPSE_VARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_CROSSMODULE_CODE, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_CROSSMODULE_METHOD, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_DEAD_ASSIGMENT, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_DOTTED_PROPERTIES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_EXTACT_PROTOTYPEMETHOD, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_FOLD_CONSTANTS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_GROUP_VARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_CONSTANTS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_FUNCTIONS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_GETTER, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_LOCALFUNCTIONS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_LOCALVARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_PROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_INLINE_VARIABLES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_OPTIMIZE_ARGUMENTS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_OPTIMIZE_CALLS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_OPTIMIZE_PARAMETERS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_OPTIMIZE_RETURNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_CLASSPROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_DEADCODE, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_SMARTNAME, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_UNUSEDLOCALVARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_UNUSEDPROTOTYPE, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_UNUSEDPROTOTYPE_EXTERNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REMOVE_UNUSEDVARIABLES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OPTIMIZATION_REWRITE_FUNCTIONEXPR, ClosureCore.DISABLED);

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_ALIAS_EXTERNALS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_ALIAS_KEYWORDS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_ALIAS_STRINGS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_ASSUME_CLOSUREONLY, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_ASSUME_STRICTTHIS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COALESCE_VARS_NAMES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COLLAPSE_ANONYMOUSFUNCTIONS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COLLAPSE_OBKECTLITERALS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COLLAPSE_PROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COLLAPSE_PROPERTIESEXTERNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_COLLAPSE_VARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_CROSSMODULE_CODE, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_CROSSMODULE_METHOD, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_DEAD_ASSIGMENT, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_DOTTED_PROPERTIES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_EXTACT_PROTOTYPEMETHOD, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_FOLD_CONSTANTS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_GROUP_VARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_CONSTANTS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_FUNCTIONS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_GETTER, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_LOCALFUNCTIONS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_LOCALVARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_PROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_INLINE_VARIABLES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_OPTIMIZE_ARGUMENTS, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_OPTIMIZE_CALLS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_OPTIMIZE_PARAMETERS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_OPTIMIZE_RETURNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_CLASSPROPERTIES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_DEADCODE, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_SMARTNAME, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_UNUSEDLOCALVARIABLES, ClosureCore.ENABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_UNUSEDPROTOTYPE, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_UNUSEDPROTOTYPE_EXTERNS, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REMOVE_UNUSEDVARIABLES, ClosureCore.DISABLED);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OPTIMIZATION_REWRITE_FUNCTIONEXPR, ClosureCore.DISABLED);


		/*
		 * CLOSURE COMPILER WARNINGS
		 */
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_ACCESS_CONTROLS, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_AGGRESSIVE_VAR, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_AMBIGUOUS_FUNCTIONDECL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_CLOSURE_PROVIDES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_CLOSURE_REQUIRES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_CLOSUREREQUIRES_LEVEL, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_CONST, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_DEBUGGER_STMT, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_DEPRECATED, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_DUBPLICATE_VARIABLES, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_ES3, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_ES5STRICT, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_EVENTFUL_OBJECT_DISPOSAL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_EXTERNS_VALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_FILE_OVERVIEW, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_GLOBAL_THIS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_GLOBALNAMES_LEVEL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_GLOBALTHIS_LEVEL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_IE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_INVALID_CASTS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_MISSING_OVERRIDE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_MISSING_PROPERTIES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_MISSING_RETURN, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_MISSPLACED_TYPE_ANNOTATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_NONSTANDARD_JSDOCS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_REGEXPR, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_STRUCT_DICT_INHERITANCE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_SUSPICIOUS_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_TWEAK_VALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_TYPE_INVALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_TYPES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNDEFINED_NAMES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNDEFINED_VARS, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNKNOWN_DEFINES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNKNOWN_TYPES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNREACHABLE_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_USELESS_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PB_UNNECESSARY_CASTS, ClosureCore.IGNORE);

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_ACCESS_CONTROLS, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_AGGRESSIVE_VAR, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_AMBIGUOUS_FUNCTIONDECL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_CLOSURE_PROVIDES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_CLOSURE_REQUIRES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_CLOSUREREQUIRES_LEVEL, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_CONST, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_DEBUGGER_STMT, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_DEPRECATED, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_DUBPLICATE_VARIABLES, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_ES3, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_ES5STRICT, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_EVENTFUL_OBJECT_DISPOSAL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_EXTERNS_VALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_FILE_OVERVIEW, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_GLOBAL_THIS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_GLOBALNAMES_LEVEL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_GLOBALTHIS_LEVEL, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_IE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_INVALID_CASTS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_MISSING_OVERRIDE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_MISSING_PROPERTIES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_MISSING_RETURN, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_MISSPLACED_TYPE_ANNOTATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_NONSTANDARD_JSDOCS, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_REGEXPR, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_STRUCT_DICT_INHERITANCE, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_SUSPICIOUS_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_TWEAK_VALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_TYPE_INVALIDATION, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_TYPES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNDEFINED_NAMES, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNDEFINED_VARS, ClosureCore.ERROR);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNKNOWN_DEFINES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNKNOWN_TYPES, ClosureCore.IGNORE);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNREACHABLE_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_USELESS_CODE, ClosureCore.WARNING);
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_PB_UNNECESSARY_CASTS, ClosureCore.IGNORE);

		/*
		 * CLOSURE CONSOLE
		 */
		defaultOptionsMap.put(ClosureCore.CONSOLE_LIMIT, String.valueOf(true));
		defaultOptionsMap.put(ClosureCore.CONSOLE_BUFFER, String.valueOf(500000));
		defaultOptionsMap.put(ClosureCore.CONSOLE_COLOR_ERROR, "128,0,0");
		defaultOptionsMap.put(ClosureCore.CONSOLE_COLOR_WARNING, "255,128,0");
		defaultOptionsMap.put(ClosureCore.CONSOLE_COLOR_INFO, "0,0,255");
		defaultOptionsMap.put(ClosureCore.CONSOLE_COLOR_BACK, "255,255,255");
		defaultOptionsMap.put(ClosureCore.CONSOLE_COLOR_TEXT, "0,0,0");
		defaultOptionsMap.put(ClosureCore.CONSOLE_SHOW_ERRORS, String.valueOf(true));
		defaultOptionsMap.put(ClosureCore.CONSOLE_SHOW_MESSAGES, String.valueOf(true));
		defaultOptionsMap.put(ClosureCore.CONSOLE_SHOW_WARNING, String.valueOf(true));

		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_OUTPUT_FILE, "output.js");
		defaultOptionsMap.put(ClosureCore.COMPILER_CLOSURE_PRODUCTION_OUTPUT_FILE, "output.js");

		// Store default values to default preferences
		IEclipsePreferences defaultPreferences = ((IScopeContext) new DefaultScope()).getNode(ClosureCore.PLUGIN_ID);
		for (Iterator iter = defaultOptionsMap.entrySet().iterator(); iter.hasNext();) {
			Map.Entry entry = (Map.Entry) iter.next();
			String optionName = (String) entry.getKey();
			defaultPreferences.put(optionName, (String) entry.getValue());
			optionNames.add(optionName);
		}
	}
}
