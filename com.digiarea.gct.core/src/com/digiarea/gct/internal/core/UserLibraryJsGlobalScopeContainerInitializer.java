/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJsGlobalScopeContainer;
import com.digiarea.gct.core.JsGlobalScopeContainerInitializer;
import com.digiarea.gct.core.compiler.libraries.LibraryLocation;

/**
 *
 */
public class UserLibraryJsGlobalScopeContainerInitializer extends JsGlobalScopeContainerInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.digiarea.gct.core.JsGlobalScopeContainerInitializer#initialize(
	 * org.eclipse.core.runtime.IPath, com.digiarea.gct.core.IClosureProject)
	 */
	public void initialize(IPath containerPath, IClosureProject project) throws CoreException {
		if (isUserLibraryContainer(containerPath)) {
			String userLibName = containerPath.segment(1);

			UserLibrary entries = UserLibraryManager.getUserLibrary(userLibName);
			if (entries != null) {
				UserLibraryJsGlobalScopeContainer container = new UserLibraryJsGlobalScopeContainer(userLibName);
				ClosureCore.setJsGlobalScopeContainer(containerPath, new IClosureProject[]{project}, new IJsGlobalScopeContainer[]{container}, null);
			}
		}
	}

	private boolean isUserLibraryContainer(IPath path) {
		return path != null && path.segmentCount() == 2 && ClosureCore.USER_LIBRARY_CONTAINER_ID.equals(path.segment(0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.digiarea.gct.core.JsGlobalScopeContainerInitializer#
	 * canUpdateJsGlobalScopeContainer(org.eclipse.core.runtime.IPath,
	 * com.digiarea.gct.core.IClosureProject)
	 */
	public boolean canUpdateJsGlobalScopeContainer(IPath containerPath, IClosureProject project) {
		return isUserLibraryContainer(containerPath);
	}

	/**
	 * @see com.digiarea.gct.core.JsGlobalScopeContainerInitializer#requestJsGlobalScopeContainerUpdate(org.eclipse.core.runtime.IPath,
	 *      com.digiarea.gct.core.IClosureProject,
	 *      com.digiarea.gct.core.IJsGlobalScopeContainer)
	 */
	public void requestJsGlobalScopeContainerUpdate(IPath containerPath, IClosureProject project, IJsGlobalScopeContainer containerSuggestion) throws CoreException {
		if (isUserLibraryContainer(containerPath)) {
			String name = containerPath.segment(1);
			if (containerSuggestion != null) {
				UserLibrary library = new UserLibrary(containerSuggestion.getIncludepathEntries(), containerSuggestion.getKind() == IJsGlobalScopeContainer.K_SYSTEM, containerSuggestion.getKind() == IJsGlobalScopeContainer.K_CLOSURE);
				UserLibraryManager.setUserLibrary(name, library, null); // should
																		// use
																		// a
																		// real
																		// progress
																		// monitor
			}
			else {
				UserLibraryManager.setUserLibrary(name, null, null); // should
																		// use
																		// a
																		// real
																		// progress
																		// monitor
			}
		}
	}

	/**
	 * @see com.digiarea.gct.core.JsGlobalScopeContainerInitializer#getDescription(org.eclipse.core.runtime.IPath,
	 *      com.digiarea.gct.core.IClosureProject)
	 */
	public String getDescription(IPath containerPath, IClosureProject project) {
		if (isUserLibraryContainer(containerPath)) {
			return containerPath.segment(1);
		}
		return super.getDescription(containerPath, project);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.digiarea.gct.core.JsGlobalScopeContainerInitializer#getComparisonID
	 * (org.eclipse.core.runtime.IPath, com.digiarea.gct.core.IClosureProject)
	 */
	public Object getComparisonID(IPath containerPath, IClosureProject project) {
		return containerPath;
	}

	public LibraryLocation getLibraryLocation() {
		return null;
	}
}
