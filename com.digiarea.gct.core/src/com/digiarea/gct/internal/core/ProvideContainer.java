/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IProvideContainer;
import com.digiarea.gct.core.IProvideDeclaration;
import com.digiarea.gct.core.IRequireContainer;
import com.digiarea.gct.core.ISourceRange;
import com.digiarea.gct.core.ISourceReference;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.WorkingCopyOwner;
import com.digiarea.gct.internal.core.util.MementoTokenizer;

/**
 * @see IRequireContainer
 */
public class ProvideContainer extends SourceRefElement implements IProvideContainer {
	protected ProvideContainer(CompilationUnit parent) {
		super(parent);
	}
	protected ProvideContainer(ClassFile parent) {
		super(parent);
	}
	public boolean equals(Object o) {
		if (!(o instanceof ProvideContainer))
			return false;
		return super.equals(o);
	}

	/**
	 * @see IJavaScriptElement
	 */
	public int getElementType() {
		return PROVIDE_CONTAINER;
	}

	/*
	 * @see JavaElement
	 */
	public IJavaScriptElement getHandleFromMemento(String token, MementoTokenizer memento, WorkingCopyOwner workingCopyOwner) {
		switch (token.charAt(0)) {
			case JEM_COUNT :
				return getHandleUpdatingCountFromMemento(memento, workingCopyOwner);
			case JEM_IMPORTDECLARATION :
				if (memento.hasMoreTokens()) {
					String importName = memento.nextToken();
					JavaElement importDecl = (JavaElement) getProvide(importName);
					return importDecl.getHandleFromMemento(memento, workingCopyOwner);
				}
				else {
					return this;
				}
		}
		return null;
	}

	/**
	 * @see JavaElement#getHandleMemento()
	 */
	protected char getHandleMementoDelimiter() {
		return JavaElement.JEM_IMPORTDECLARATION;
	}

	/**
	 * @see IProvideContainer
	 */
	public IProvideDeclaration getProvide(String importName) {
		return new ProvideDeclaration(this, importName, true);
	}

	/*
	 * @see JavaElement#getPrimaryElement(boolean)
	 */
	public IJavaScriptElement getPrimaryElement(boolean checkOwner) {
		CompilationUnit cu = (CompilationUnit) this.parent;
		if (checkOwner && cu.isPrimary())
			return this;
		return cu.getRequireContainer();
	}

	/**
	 * @see ISourceReference
	 */
	public ISourceRange getSourceRange() throws JavaScriptModelException {
		IJavaScriptElement[] imports = getChildren();
		ISourceRange firstRange = ((ISourceReference) imports[0]).getSourceRange();
		ISourceRange lastRange = ((ISourceReference) imports[imports.length - 1]).getSourceRange();
		SourceRange range = new SourceRange(firstRange.getOffset(), lastRange.getOffset() + lastRange.getLength() - firstRange.getOffset());
		return range;
	}

	/**
 */
	public String readableName() {

		return null;
	}

	/**
	 * @private Debugging purposes
	 */
	protected void toString(int tab, StringBuffer buffer) {
		Object info = JavaModelManager.getJavaModelManager().peekAtInfo(this);
		if (info == null || !(info instanceof JavaElementInfo))
			return;
		IJavaScriptElement[] children = ((JavaElementInfo) info).getChildren();
		for (int i = 0; i < children.length; i++) {
			if (i > 0)
				buffer.append("\n"); //$NON-NLS-1$
			((JavaElement) children[i]).toString(tab, buffer);
		}
	}

	/**
	 * Debugging purposes
	 */
	protected void toStringInfo(int tab, StringBuffer buffer, Object info, boolean showResolvedInfo) {
		buffer.append(this.tabString(tab));
		buffer.append("<provide container>"); //$NON-NLS-1$
		if (info == null) {
			buffer.append(" (not open)"); //$NON-NLS-1$
		}
	}
}
