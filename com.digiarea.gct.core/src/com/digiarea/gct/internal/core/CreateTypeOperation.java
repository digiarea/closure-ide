/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

import org.eclipse.jface.text.IDocument;

import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IJavaScriptModelStatus;
import com.digiarea.gct.core.IJavaScriptModelStatusConstants;
import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.IType;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.AbstractTypeDeclaration;
import com.digiarea.gct.core.dom.Assignment;
import com.digiarea.gct.core.dom.ExpressionStatement;
import com.digiarea.gct.core.dom.FieldAccess;
import com.digiarea.gct.core.dom.FunctionDeclaration;
import com.digiarea.gct.core.dom.Name;
import com.digiarea.gct.core.dom.SimpleName;
import com.digiarea.gct.core.dom.rewrite.ASTRewrite;
import com.digiarea.gct.internal.core.util.Messages;

/**
 * <p>
 * This operation creates a class or interface.
 * 
 * <p>
 * Required Attributes:
 * <ul>
 * <li>Parent element - must be a compilation unit, or type.
 * <li>The source code for the type. No verification of the source is
 * performed.
 * </ul>
 */
public class CreateTypeOperation extends CreateTypeMemberOperation {
	/**
	 * When executed, this operation will create a type unit in the given
	 * parent element (a compilation unit, type)
	 */
	public CreateTypeOperation(IJavaScriptElement parentElement, String source, boolean force) {
		super(parentElement, source, force);
	}

	protected ASTNode generateElementAST(ASTRewrite rewriter, IDocument document, IJavaScriptUnit cu) throws JavaScriptModelException {
		ASTNode node = super.generateElementAST(rewriter, document, cu);
		// if (!(node instanceof AbstractTypeDeclaration))
		// throw new JavaScriptModelException(new
		// JavaModelStatus(IJavaScriptModelStatusConstants.INVALID_CONTENTS));
		return node;
	}

	/**
	 * @see CreateElementInCUOperation#generateResultHandle()
	 */
	protected IJavaScriptElement generateResultHandle() {
		IJavaScriptElement parent = getParentElement();
		switch (parent.getElementType()) {
			case IJavaScriptElement.JAVASCRIPT_UNIT :
				return ((IJavaScriptUnit) parent).getType(getASTNodeName());
			case IJavaScriptElement.TYPE :
				return ((IType) parent).getType(getASTNodeName());
				// Note: creating local/anonymous type is not supported
		}
		return null;
	}

	/**
	 * @see CreateElementInCUOperation#getMainTaskName()
	 */
	public String getMainTaskName() {
		return Messages.operation_createTypeProgress;
	}

	/**
	 * Returns the <code>IType</code> the member is to be created in.
	 */
	protected IType getType() {
		IJavaScriptElement parent = getParentElement();
		if (parent.getElementType() == IJavaScriptElement.TYPE) {
			return (IType) parent;
		}
		return null;
	}

	/**
	 * @see CreateTypeMemberOperation#verifyNameCollision
	 */
	protected IJavaScriptModelStatus verifyNameCollision() {
		IJavaScriptElement parent = getParentElement();
		switch (parent.getElementType()) {
			case IJavaScriptElement.JAVASCRIPT_UNIT :
				String typeName = getASTNodeName();
				if (((IJavaScriptUnit) parent).getType(typeName).exists()) {
					return new JavaModelStatus(IJavaScriptModelStatusConstants.NAME_COLLISION, Messages.bind(Messages.status_nameCollision, typeName));
				}
				break;
			case IJavaScriptElement.TYPE :
				typeName = getASTNodeName();
				if (((IType) parent).getType(typeName).exists()) {
					return new JavaModelStatus(IJavaScriptModelStatusConstants.NAME_COLLISION, Messages.bind(Messages.status_nameCollision, typeName));
				}
				break;
		// Note: creating local/anonymous type is not supported
		}
		return JavaModelStatus.VERIFIED_OK;
	}

	private String getASTNodeName() {
		String name = "";
		ASTNode node = this.createdNode;
		if(node.getNodeType() == ASTNode.EXPRESSION_STATEMENT){
			node = ((ExpressionStatement) node).getExpression();
		}
		
		if (node.getNodeType() == ASTNode.FUNCTION_DECLARATION) {
			name= ((FunctionDeclaration) node).getName().getIdentifier();
		}
		else if (node.getNodeType() == ASTNode.ASSIGNMENT) {
			if (((Assignment) node).getLeftHandSide() instanceof Name) {
				name= ((Name) ((Assignment) node).getLeftHandSide()).getFullyQualifiedName();
			}else if (((Assignment) node).getLeftHandSide() instanceof FieldAccess) {
				name= ((FieldAccess) ((Assignment) node).getLeftHandSide()).toString();
			}
		}
		else if (node instanceof AbstractTypeDeclaration) {
			name= ((AbstractTypeDeclaration) node).getName().getIdentifier();
		}
		return name;
	}

	protected SimpleName rename(ASTNode node, SimpleName newName) {
		AbstractTypeDeclaration type = (AbstractTypeDeclaration) node;
		SimpleName oldName = type.getName();
		type.setName(newName);
		return oldName;
	}
}
