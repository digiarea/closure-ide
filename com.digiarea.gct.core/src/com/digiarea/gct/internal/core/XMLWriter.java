/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;
import java.io.Writer;

import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.internal.compiler.util.GenericXMLWriter;
import com.digiarea.gct.internal.core.util.Util;
/**
 * @since 3.0
 */
public class XMLWriter extends GenericXMLWriter {

	public XMLWriter(Writer writer, IClosureProject project, boolean printXmlVersion) {
		super(writer, Util.getLineSeparator((String) null, project), printXmlVersion);
	}
}
