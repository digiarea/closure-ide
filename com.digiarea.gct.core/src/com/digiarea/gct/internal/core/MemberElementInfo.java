/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core;

/**
 *Element info for IMember elements.
 */
/* package */ abstract class MemberElementInfo extends SourceRefElementInfo {
	/**
	 * The modifiers associated with this member.
	 *
	 * @see com.digiarea.gct.internal.compiler.classfmt.ClassFileConstants
	 */
	protected int flags;

	/**
	 * The start position of this member's name in the its
	 * openable's buffer.
	 */
	protected int nameStart= -1;

	/**
	 * The last position of this member's name in the its
	 * openable's buffer.
	 */
	protected int nameEnd= -1;

	/**
	 * @see com.digiarea.gct.internal.compiler.env.IGenericType#getModifiers()
	 * @see com.digiarea.gct.internal.compiler.env.IGenericMethod#getModifiers()
	 * @see com.digiarea.gct.internal.compiler.env.IGenericField#getModifiers()
	 */
	public int getModifiers() {
		return this.flags;
	}
	/**
	 * @see com.digiarea.gct.internal.compiler.env.ISourceType#getNameSourceEnd()
	 * @see com.digiarea.gct.internal.compiler.env.ISourceMethod#getNameSourceEnd()
	 * @see com.digiarea.gct.internal.compiler.env.ISourceField#getNameSourceEnd()
	 */
	public int getNameSourceEnd() {
		return this.nameEnd;
	}
	/**
	 * @see com.digiarea.gct.internal.compiler.env.ISourceType#getNameSourceStart()
	 * @see com.digiarea.gct.internal.compiler.env.ISourceMethod#getNameSourceStart()
	 * @see com.digiarea.gct.internal.compiler.env.ISourceField#getNameSourceStart()
	 */
	public int getNameSourceStart() {
		return this.nameStart;
	}
	protected void setFlags(int flags) {
		this.flags = flags;
	}
	/**
	 * Sets the last position of this member's name, relative
	 * to its openable's source buffer.
	 */
	protected void setNameSourceEnd(int end) {
		this.nameEnd= end;
	}
	/**
	 * Sets the start position of this member's name, relative
	 * to its openable's source buffer.
	 */
	protected void setNameSourceStart(int start) {
		this.nameStart= start;
	}
}
