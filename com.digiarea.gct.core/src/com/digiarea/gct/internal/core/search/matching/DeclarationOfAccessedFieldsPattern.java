/*******************************************************************************
 * Copyright (c) 2000, 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.core.search.matching;

import com.digiarea.gct.core.IField;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.internal.compiler.util.SimpleSet;

public class DeclarationOfAccessedFieldsPattern extends FieldPattern {

protected IJavaScriptElement enclosingElement;
protected SimpleSet knownFields;

public DeclarationOfAccessedFieldsPattern(IJavaScriptElement enclosingElement) {
	super(false, true, true, false,null, null, null, null, R_PATTERN_MATCH,
			(enclosingElement instanceof IField) ?(IField)enclosingElement :null);

	this.enclosingElement = enclosingElement;
	this.knownFields = new SimpleSet();
	((InternalSearchPattern)this).mustResolve = true;
}
}
