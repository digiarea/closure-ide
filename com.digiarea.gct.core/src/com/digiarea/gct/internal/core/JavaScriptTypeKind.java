/*******************************************************************************
 * Licensed Materials - Property of DigiArea, Inc.
 * � Copyright DigiArea Corporation 2014. All Rights Reserved.
 *******************************************************************************/

package com.digiarea.gct.internal.core;

/**
 * @author daginno
 *
 */
public enum JavaScriptTypeKind {
	
	TYPE, NAMESPACE, OBJECT

}
