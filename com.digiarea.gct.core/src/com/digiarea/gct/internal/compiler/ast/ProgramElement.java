/*******************************************************************************
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.compiler.ast;

import com.digiarea.gct.core.ast.IASTNode;
import com.digiarea.gct.core.ast.IProgramElement;
import com.digiarea.gct.internal.compiler.lookup.BlockScope;
import com.digiarea.gct.internal.compiler.lookup.Scope;

public abstract class ProgramElement extends ASTNode implements IProgramElement {

	public abstract StringBuffer printStatement(int indent, StringBuffer output);

	public void resolve(BlockScope scope)
	{
		if (this instanceof AbstractMethodDeclaration)
			((AbstractMethodDeclaration)this).resolve((Scope)scope);
		else
			//TODO: implement
			throw new com.digiarea.gct.core.UnimplementedException();
	}
	public int getASTType() {
		return IASTNode.PROGRAM_ELEMENT;
	
	}

}
