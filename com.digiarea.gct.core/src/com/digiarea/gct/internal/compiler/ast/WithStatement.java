/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.compiler.ast;

import com.digiarea.gct.core.ast.IASTNode;
import com.digiarea.gct.core.ast.IWithStatement;
import com.digiarea.gct.internal.compiler.ASTVisitor;
import com.digiarea.gct.internal.compiler.flow.FlowContext;
import com.digiarea.gct.internal.compiler.flow.FlowInfo;
import com.digiarea.gct.internal.compiler.lookup.BlockScope;
import com.digiarea.gct.internal.compiler.lookup.ReferenceBinding;
import com.digiarea.gct.internal.compiler.lookup.TypeBinding;
import com.digiarea.gct.internal.compiler.lookup.WithScope;

public class WithStatement extends Statement implements IWithStatement {

	public Expression condition;
	public Statement action;

	public WithStatement(Expression condition, Statement action, int s, int e) {
		this.condition = condition;
		this.action = action;
		// remember useful empty statement
		if (action instanceof EmptyStatement) action.bits |= IsUsefulEmptyStatement;
		sourceStart = s;
		sourceEnd = e;
	}

	public FlowInfo analyseCode( BlockScope currentScope, FlowContext flowContext,
		FlowInfo flowInfo) {

		flowInfo =
			condition.analyseCode(currentScope, flowContext, flowInfo);

		if (this.action != null) {
			if (!action.complainIfUnreachable(flowInfo, currentScope, false)) {
				flowInfo =
					this.action.analyseCode(currentScope, flowContext, flowInfo);
			}
		}
		return flowInfo;
	}

	public void resolve(BlockScope parentScope) {

		TypeBinding type = condition.resolveTypeExpecting(parentScope, TypeBinding.ANY);
        BlockScope scope = (type instanceof ReferenceBinding)?
        		new WithScope(parentScope,(ReferenceBinding)type) : parentScope;
		if (action != null)
			action.resolve(scope);
	}

	public StringBuffer printStatement(int tab, StringBuffer output) {

		printIndent(tab, output).append("with ("); //$NON-NLS-1$
		condition.printExpression(0, output).append(")\n"); //$NON-NLS-1$
		if (action == null)
			output.append(';');
		else
			action.printStatement(tab + 1, output);
		return output;
	}

	public void traverse(
		ASTVisitor visitor,
		BlockScope blockScope) {

		if (visitor.visit(this, blockScope)) {
			condition.traverse(visitor, blockScope);
			if (action != null)
				action.traverse(visitor, blockScope);
		}
		visitor.endVisit(this, blockScope);
	}
	
	public int getASTType() {
		return IASTNode.WITH_STATEMENT;
	}
}
