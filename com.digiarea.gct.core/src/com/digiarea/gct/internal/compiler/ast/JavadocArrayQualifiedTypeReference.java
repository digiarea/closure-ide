/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.compiler.ast;

import com.digiarea.gct.core.ast.IASTNode;
import com.digiarea.gct.core.ast.IJsDocArrayQualifiedTypeReference;
import com.digiarea.gct.internal.compiler.ASTVisitor;
import com.digiarea.gct.internal.compiler.lookup.BlockScope;
import com.digiarea.gct.internal.compiler.lookup.ClassScope;
import com.digiarea.gct.internal.compiler.lookup.Scope;
import com.digiarea.gct.internal.compiler.lookup.TypeBinding;



public class JavadocArrayQualifiedTypeReference extends ArrayQualifiedTypeReference implements IJsDocArrayQualifiedTypeReference {

	public int tagSourceStart, tagSourceEnd;

	public JavadocArrayQualifiedTypeReference(JavadocQualifiedTypeReference typeRef, int dim) {
		super(typeRef.tokens, dim, typeRef.sourcePositions);
	}

	protected void reportInvalidType(Scope scope) {
		scope.problemReporter().javadocInvalidType(this, this.resolvedType, scope.getDeclarationModifiers());
	}
	protected void reportDeprecatedType(TypeBinding type, Scope scope) {
		scope.problemReporter().javadocDeprecatedType(type, this, scope.getDeclarationModifiers());
	}

	/* (non-Javadoc)
	 * Redefine to capture javadoc specific signatures
	 * @see com.digiarea.gct.internal.compiler.ast.ASTNode#traverse(com.digiarea.gct.internal.compiler.ASTVisitor, com.digiarea.gct.internal.compiler.lookup.BlockScope)
	 */
	public void traverse(ASTVisitor visitor, BlockScope scope) {
		visitor.visit(this, scope);
		visitor.endVisit(this, scope);
	}

	public void traverse(ASTVisitor visitor, ClassScope scope) {
		visitor.visit(this, scope);
		visitor.endVisit(this, scope);
	}
	public int getASTType() {
		return IASTNode.JSDOC_ARRAY_QUALIFIED_TYPE_REFERENCE;
	
	}
}
