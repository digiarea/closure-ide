/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.ui.actions;

import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.PlatformUI;

import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.internal.corext.refactoring.RefactoringAvailabilityTester;
import com.digiarea.gct.internal.corext.refactoring.code.PromoteTempToFieldRefactoring;
import com.digiarea.gct.internal.ui.IJavaHelpContextIds;
import com.digiarea.gct.internal.ui.actions.ActionUtil;
import com.digiarea.gct.internal.ui.actions.SelectionConverter;
import com.digiarea.gct.internal.ui.javaeditor.JavaEditor;
import com.digiarea.gct.internal.ui.javaeditor.JavaTextSelection;
import com.digiarea.gct.internal.ui.refactoring.PromoteTempWizard;
import com.digiarea.gct.internal.ui.refactoring.RefactoringMessages;
import com.digiarea.gct.internal.ui.refactoring.RefactoringSaveHelper;
import com.digiarea.gct.internal.ui.refactoring.actions.RefactoringStarter;
import com.digiarea.gct.internal.ui.util.ExceptionHandler;

/**
 * Action to convert a local variable to a field.
 * <p>
 * This class may be instantiated; it is not intended to be subclassed.
 * </p>
 * 
 *
 * Provisional API: This class/interface is part of an interim API that is still under development and expected to
 * change significantly before reaching stability. It is being made available at this early stage to solicit feedback
 * from pioneering adopters on the understanding that any code that uses this API will almost certainly be broken
 * (repeatedly) as the API evolves.
 */
public class ConvertLocalToFieldAction extends SelectionDispatchAction {

	private final JavaEditor fEditor;
	
	/**
	 * Note: This constructor is for internal use only. Clients should not call this constructor.
	 * 
	 * @param editor the JavaScript editor
	 */
	public ConvertLocalToFieldAction(JavaEditor editor) {
		super(editor.getEditorSite());
		setText(RefactoringMessages.ConvertLocalToField_label); 
		fEditor= editor;
		setEnabled(SelectionConverter.getInputAsCompilationUnit(fEditor) != null);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(this, IJavaHelpContextIds.PROMOTE_TEMP_TO_FIELD_ACTION);
	}

	/* (non-Javadoc)
	 * Method declared on SelectionDispatchAction
	 */		
	public void selectionChanged(ITextSelection selection) {
		setEnabled(fEditor != null && SelectionConverter.getInputAsCompilationUnit(fEditor) != null);
	}
	
	/**
	 * Note: This method is for internal use only. Clients should not call this method.
	 */
	public void selectionChanged(JavaTextSelection selection) {
		try {
			setEnabled(RefactoringAvailabilityTester.isPromoteTempAvailable(selection));
		} catch (JavaScriptModelException e) {
			setEnabled(false);
		}
	}
	
	/* (non-Javadoc)
	 * Method declared on SelectionDispatchAction
	 */		
	public void run(ITextSelection selection) {
		if (!ActionUtil.isEditable(fEditor))
			return;
		try{
			IJavaScriptUnit cunit= SelectionConverter.getInputAsCompilationUnit(fEditor);
			final PromoteTempToFieldRefactoring refactoring= new PromoteTempToFieldRefactoring(cunit, selection.getOffset(), selection.getLength());
			new RefactoringStarter().activate(refactoring, new PromoteTempWizard(refactoring), getShell(), RefactoringMessages.ConvertLocalToField_title, RefactoringSaveHelper.SAVE_NOTHING);
		} catch (JavaScriptModelException e){
			ExceptionHandler.handle(e, RefactoringMessages.ConvertLocalToField_title, RefactoringMessages.NewTextRefactoringAction_exception); 
		}	
	}
}
