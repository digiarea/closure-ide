/*******************************************************************************
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package com.digiarea.gct.ui;

import java.util.regex.Pattern;

import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.rewrite.ProvideRewrite;
import com.digiarea.gct.core.dom.rewrite.RequireRewrite;

/**
 *
 * Provisional API: This class/interface is part of an interim API that is
 * still under development and expected to change significantly before
 * reaching stability. It is being made available at this early stage to
 * solicit feedback from pioneering adopters on the understanding that any
 * code that uses this API will almost certainly be broken (repeatedly) as the
 * API evolves.
 */
public class CodeStyleConfiguration {

	private static final Pattern SEMICOLON_PATTERN = Pattern.compile(";"); //$NON-NLS-1$

	private CodeStyleConfiguration() {
		// do not instantiate and subclass
	}


	/**
	 * Returns a {@link RequireRewrite} using
	 * {@link RequireRewrite#create(IJavaScriptUnit, boolean)} and configures
	 * the rewriter with the settings as specified in the JDT UI preferences.
	 * <p>
	 * 
	 * @param cu
	 *            the compilation unit to create the rewriter on
	 * @param restoreExistingImports
	 *            specifies if the existing imports should be kept or removed.
	 * @return the new rewriter configured with the settings as specified in
	 *         the JDT UI preferences.
	 * @throws JavaScriptModelException
	 *             thrown when the compilation unit could not be accessed.
	 * 
	 * @see RequireRewrite#create(IJavaScriptUnit, boolean)
	 */
	public static RequireRewrite createImportRewrite(IJavaScriptUnit cu, boolean restoreExistingImports) throws JavaScriptModelException {
		return configureImportRewrite(RequireRewrite.create(cu, restoreExistingImports));
	}

	/**
	 * Returns a {@link RequireRewrite} using
	 * {@link RequireRewrite#create(JavaScriptUnit, boolean)} and configures
	 * the rewriter with the settings as specified in the JDT UI preferences.
	 * 
	 * @param astRoot
	 *            the AST root to create the rewriter on
	 * @param restoreExistingImports
	 *            specifies if the existing imports should be kept or removed.
	 * @return the new rewriter configured with the settings as specified in
	 *         the JDT UI preferences.
	 * 
	 * @see RequireRewrite#create(JavaScriptUnit, boolean)
	 */
	public static RequireRewrite createImportRewrite(JavaScriptUnit astRoot, boolean restoreExistingImports) {
		return configureImportRewrite(RequireRewrite.create(astRoot, restoreExistingImports));
	}

	public static ProvideRewrite createProvideRewrite(IJavaScriptUnit unit) {
		return configureProvideRewrite(ProvideRewrite.create(unit));
	}

	/**
	 * @param create
	 * @return
	 */
	private static ProvideRewrite configureProvideRewrite(ProvideRewrite create) {
		return create;
	}


	private static RequireRewrite configureImportRewrite(RequireRewrite rewrite) {
		IClosureProject project = rewrite.getCompilationUnit().getJavaScriptProject();
		String order = PreferenceConstants.getPreference(PreferenceConstants.ORGIMPORTS_IMPORTORDER, project);
		rewrite.setImportOrder(SEMICOLON_PATTERN.split(order, 0));

		String thres = PreferenceConstants.getPreference(PreferenceConstants.ORGIMPORTS_ONDEMANDTHRESHOLD, project);
		try {
			int num = Integer.parseInt(thres);
			if (num == 0)
				num = 1;
			rewrite.setOnDemandImportThreshold(num);
		}
		catch (NumberFormatException e) {
			// ignore
		}
		String thresStatic = PreferenceConstants.getPreference(PreferenceConstants.ORGIMPORTS_STATIC_ONDEMANDTHRESHOLD, project);
		try {
			int num = Integer.parseInt(thresStatic);
			if (num == 0)
				num = 1;
			rewrite.setStaticOnDemandImportThreshold(num);
		}
		catch (NumberFormatException e) {
			// ignore
		}
		return rewrite;
	}



}
