/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.code;

import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.Expression;
import com.digiarea.gct.core.dom.IFunctionBinding;
import com.digiarea.gct.core.dom.ITypeBinding;
import com.digiarea.gct.core.dom.rewrite.RequireRewrite;
import com.digiarea.gct.internal.corext.dom.CodeScopeBuilder;

public class CallContext {

	public ASTNode invocation;
	public String[] arguments;
	public String receiver; 
	public boolean receiverIsStatic;
	public CodeScopeBuilder.Scope scope;
	public int callMode;
	public RequireRewrite importer;

	public CallContext(ASTNode inv, CodeScopeBuilder.Scope s, int cm, RequireRewrite i) {
		super();
		invocation= inv;
		scope= s;
		callMode= cm;
		importer= i;
	}
	
	public ITypeBinding getReceiverType() {
		Expression expression= Invocations.getExpression(invocation);
		if (expression != null) {
			return expression.resolveTypeBinding();
		}
		IFunctionBinding method= Invocations.resolveBinding(invocation);
		if (method != null) {
			return method.getDeclaringClass();
		}
		return null;
	}
}
