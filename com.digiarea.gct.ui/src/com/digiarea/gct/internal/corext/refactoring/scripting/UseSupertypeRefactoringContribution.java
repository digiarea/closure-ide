/*******************************************************************************
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.scripting;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;

import com.digiarea.gct.core.refactoring.descriptors.UseSupertypeDescriptor;
import com.digiarea.gct.internal.corext.refactoring.JDTRefactoringContribution;
import com.digiarea.gct.internal.corext.refactoring.structure.UseSuperTypeProcessor;
import com.digiarea.gct.internal.corext.refactoring.structure.UseSuperTypeRefactoring;

/**
 * Refactoring contribution for the use supertype refactoring.
 * 
 * 
 */
public final class UseSupertypeRefactoringContribution extends JDTRefactoringContribution {

	/**
	 * {@inheritDoc}
	 */
	public final Refactoring createRefactoring(final RefactoringDescriptor descriptor) throws CoreException {
		return new UseSuperTypeRefactoring(new UseSuperTypeProcessor(null, null));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public RefactoringDescriptor createDescriptor() {
		return new UseSupertypeDescriptor();
	}
}
