/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.typeconstraints;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.runtime.Assert;

import com.digiarea.gct.core.dom.ASTVisitor;
import com.digiarea.gct.core.dom.AnonymousClassDeclaration;
import com.digiarea.gct.core.dom.ArrayAccess;
import com.digiarea.gct.core.dom.ArrayCreation;
import com.digiarea.gct.core.dom.ArrayInitializer;
import com.digiarea.gct.core.dom.ArrayType;
import com.digiarea.gct.core.dom.Assignment;
import com.digiarea.gct.core.dom.Block;
import com.digiarea.gct.core.dom.BooleanLiteral;
import com.digiarea.gct.core.dom.BreakStatement;
import com.digiarea.gct.core.dom.CatchClause;
import com.digiarea.gct.core.dom.ClassInstanceCreation;
import com.digiarea.gct.core.dom.ConditionalExpression;
import com.digiarea.gct.core.dom.ConstructorInvocation;
import com.digiarea.gct.core.dom.ContinueStatement;
import com.digiarea.gct.core.dom.DoStatement;
import com.digiarea.gct.core.dom.EmptyStatement;
import com.digiarea.gct.core.dom.ExpressionStatement;
import com.digiarea.gct.core.dom.FieldAccess;
import com.digiarea.gct.core.dom.FieldDeclaration;
import com.digiarea.gct.core.dom.ForInStatement;
import com.digiarea.gct.core.dom.ForStatement;
import com.digiarea.gct.core.dom.FunctionDeclaration;
import com.digiarea.gct.core.dom.FunctionExpression;
import com.digiarea.gct.core.dom.FunctionInvocation;
import com.digiarea.gct.core.dom.IfStatement;
import com.digiarea.gct.core.dom.ImportDeclaration;
import com.digiarea.gct.core.dom.InfixExpression;
import com.digiarea.gct.core.dom.Initializer;
import com.digiarea.gct.core.dom.InstanceofExpression;
import com.digiarea.gct.core.dom.JSdoc;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.LabeledStatement;
import com.digiarea.gct.core.dom.ListExpression;
import com.digiarea.gct.core.dom.NullLiteral;
import com.digiarea.gct.core.dom.NumberLiteral;
import com.digiarea.gct.core.dom.ObjectLiteral;
import com.digiarea.gct.core.dom.ObjectLiteralField;
import com.digiarea.gct.core.dom.PackageDeclaration;
import com.digiarea.gct.core.dom.ParenthesizedExpression;
import com.digiarea.gct.core.dom.PostfixExpression;
import com.digiarea.gct.core.dom.PrefixExpression;
import com.digiarea.gct.core.dom.PrimitiveType;
import com.digiarea.gct.core.dom.QualifiedName;
import com.digiarea.gct.core.dom.RegularExpressionLiteral;
import com.digiarea.gct.core.dom.ReturnStatement;
import com.digiarea.gct.core.dom.SimpleName;
import com.digiarea.gct.core.dom.SimpleType;
import com.digiarea.gct.core.dom.SingleVariableDeclaration;
import com.digiarea.gct.core.dom.StringLiteral;
import com.digiarea.gct.core.dom.SuperConstructorInvocation;
import com.digiarea.gct.core.dom.SuperFieldAccess;
import com.digiarea.gct.core.dom.SuperMethodInvocation;
import com.digiarea.gct.core.dom.SwitchCase;
import com.digiarea.gct.core.dom.SwitchStatement;
import com.digiarea.gct.core.dom.ThisExpression;
import com.digiarea.gct.core.dom.ThrowStatement;
import com.digiarea.gct.core.dom.TryStatement;
import com.digiarea.gct.core.dom.TypeDeclaration;
import com.digiarea.gct.core.dom.TypeDeclarationStatement;
import com.digiarea.gct.core.dom.TypeLiteral;
import com.digiarea.gct.core.dom.UndefinedLiteral;
import com.digiarea.gct.core.dom.VariableDeclarationExpression;
import com.digiarea.gct.core.dom.VariableDeclarationFragment;
import com.digiarea.gct.core.dom.VariableDeclarationStatement;
import com.digiarea.gct.core.dom.WhileStatement;
import com.digiarea.gct.core.dom.WithStatement;


public final class ConstraintCollector extends ASTVisitor {

	private final ConstraintCreator fCreator;
	private final Set fConstraints;
	
	public ConstraintCollector() {
		this(new FullConstraintCreator());
	}
	
	public ConstraintCollector(ConstraintCreator creator) {
		Assert.isNotNull(creator);
		fCreator= creator;
		fConstraints= new LinkedHashSet();
	}

	private void add(ITypeConstraint[] constraints){
		fConstraints.addAll(Arrays.asList(constraints));
	}
	
	public void clear(){
		fConstraints.clear();
	}

	public ITypeConstraint[] getConstraints(){
		return (ITypeConstraint[]) fConstraints.toArray(new ITypeConstraint[fConstraints.size()]);
	}
	
	//------------------------- visit methods -------------------------//
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.AnonymousClassDeclaration)
	 */
	public boolean visit(AnonymousClassDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayAccess)
	 */
	public boolean visit(ArrayAccess node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayCreation)
	 */
	public boolean visit(ArrayCreation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayInitializer)
	 */
	public boolean visit(ArrayInitializer node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayType)
	 */
	public boolean visit(ArrayType node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Assignment)
	 */
	public boolean visit(Assignment node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Block)
	 */
	public boolean visit(Block node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.BooleanLiteral)
	 */
	public boolean visit(BooleanLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.BreakStatement)
	 */
	public boolean visit(BreakStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.CatchClause)
	 */
	public boolean visit(CatchClause node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.CharacterLiteral)
	 */
	public boolean visit(RegularExpressionLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ClassInstanceCreation)
	 */
	public boolean visit(ClassInstanceCreation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.JavaScriptUnit)
	 */
	public boolean visit(JavaScriptUnit node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ConditionalExpression)
	 */
	public boolean visit(ConditionalExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ConstructorInvocation)
	 */
	public boolean visit(ConstructorInvocation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ContinueStatement)
	 */
	public boolean visit(ContinueStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.DoStatement)
	 */
	public boolean visit(DoStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.EmptyStatement)
	 */
	public boolean visit(EmptyStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ExpressionStatement)
	 */
	public boolean visit(ExpressionStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FieldAccess)
	 */
	public boolean visit(FieldAccess node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FieldDeclaration)
	 */
	public boolean visit(FieldDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ForStatement)
	 */
	public boolean visit(ForStatement node) {
		add(fCreator.create(node));
		return true;
	}
	public boolean visit(FunctionExpression node) {
		add(fCreator.create(node));
		return true;
	}
	public boolean visit(ObjectLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	public boolean visit(ObjectLiteralField node) {
		add(fCreator.create(node));
		return true;
	}
	public boolean visit(ForInStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.IfStatement)
	 */
	public boolean visit(IfStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ImportDeclaration)
	 */
	public boolean visit(ImportDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.InfixExpression)
	 */
	public boolean visit(InfixExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Initializer)
	 */
	public boolean visit(Initializer node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.InstanceofExpression)
	 */
	public boolean visit(InstanceofExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Javadoc)
	 */
	public boolean visit(JSdoc node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.LabeledStatement)
	 */
	public boolean visit(LabeledStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	public boolean visit(ListExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FunctionDeclaration)
	 */
	public boolean visit(FunctionDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FunctionInvocation)
	 */
	public boolean visit(FunctionInvocation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.NullLiteral)
	 */
	public boolean visit(NullLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	public boolean visit(UndefinedLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.NumberLiteral)
	 */
	public boolean visit(NumberLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PackageDeclaration)
	 */
	public boolean visit(PackageDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ParenthesizedExpression)
	 */
	public boolean visit(ParenthesizedExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PostfixExpression)
	 */
	public boolean visit(PostfixExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PrefixExpression)
	 */
	public boolean visit(PrefixExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PrimitiveType)
	 */
	public boolean visit(PrimitiveType node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.QualifiedName)
	 */
	public boolean visit(QualifiedName node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ReturnStatement)
	 */
	public boolean visit(ReturnStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SimpleName)
	 */
	public boolean visit(SimpleName node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SimpleType)
	 */
	public boolean visit(SimpleType node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SingleVariableDeclaration)
	 */
	public boolean visit(SingleVariableDeclaration node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.StringLiteral)
	 */
	public boolean visit(StringLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperConstructorInvocation)
	 */
	public boolean visit(SuperConstructorInvocation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperFieldAccess)
	 */
	public boolean visit(SuperFieldAccess node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperMethodInvocation)
	 */
	public boolean visit(SuperMethodInvocation node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SwitchCase)
	 */
	public boolean visit(SwitchCase node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SwitchStatement)
	 */
	public boolean visit(SwitchStatement node) {
		add(fCreator.create(node));
		return true;
	}
	

	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ThisExpression)
	 */
	public boolean visit(ThisExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ThrowStatement)
	 */
	public boolean visit(ThrowStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TryStatement)
	 */
	public boolean visit(TryStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeDeclaration)
	 */
	public boolean visit(TypeDeclaration node) {
		add(fCreator.create(node));
		return true;
		
		// TODO account for enums and annotations
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeDeclarationStatement)
	 */
	public boolean visit(TypeDeclarationStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeLiteral)
	 */
	public boolean visit(TypeLiteral node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationExpression)
	 */
	public boolean visit(VariableDeclarationExpression node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationFragment)
	 */
	public boolean visit(VariableDeclarationFragment node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationStatement)
	 */
	public boolean visit(VariableDeclarationStatement node) {
		add(fCreator.create(node));
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.WhileStatement)
	 */
	public boolean visit(WhileStatement node) {
		add(fCreator.create(node));
		return true;
	}	
	
	public boolean visit(WithStatement node) {
		add(fCreator.create(node));
		return true;
	}
}
