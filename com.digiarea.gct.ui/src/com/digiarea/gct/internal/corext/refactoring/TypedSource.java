/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;

import com.digiarea.gct.core.IBuffer;
import com.digiarea.gct.core.IField;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.IRequireContainer;
import com.digiarea.gct.core.ISourceReference;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.dom.AST;
import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.ASTParser;
import com.digiarea.gct.core.dom.FieldDeclaration;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.VariableDeclarationFragment;
import com.digiarea.gct.internal.corext.refactoring.reorg.ReorgUtils;
import com.digiarea.gct.internal.corext.refactoring.structure.ASTNodeSearchUtil;
import com.digiarea.gct.internal.corext.util.Strings;

/**
 * A tuple used to keep source of an element and its type.
 * @see IJavaScriptElement
 * @see com.digiarea.gct.core.ISourceReference
 */
public class TypedSource {

	private static class SourceTuple {

		private SourceTuple(IJavaScriptUnit unit) {
			this.unit= unit;
		}
		private IJavaScriptUnit unit;
		private JavaScriptUnit node;
	}

	private final String fSource;
	private final int fType;

	private TypedSource(String source, int type){
		Assert.isNotNull(source);
		Assert.isTrue(canCreateForType(type));
		fSource= source;
		fType= type;				  
	}
	
	public static TypedSource create(String source, int type) {
		if (source == null || ! canCreateForType(type))
			return null;
		return new TypedSource(source, type);
	}

	public String getSource() {
		return fSource;
	}

	public int getType() {
		return fType;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (! (other instanceof TypedSource))
			return false;
		
		TypedSource ts= (TypedSource)other;
		return ts.getSource().equals(getSource()) && ts.getType() == getType();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return getSource().hashCode() ^ (97 * getType());
	}

	private static boolean canCreateForType(int type){
		return 		type == IJavaScriptElement.FIELD 
				|| 	type == IJavaScriptElement.TYPE
				|| 	type == IJavaScriptElement.REQUIRE_CONTAINER
				|| 	type == IJavaScriptElement.REQUIRE_DECLARATION
				|| 	type == IJavaScriptElement.INITIALIZER
				|| 	type == IJavaScriptElement.METHOD;
	}
	
	
	public static void sortByType(TypedSource[] typedSources){
		Arrays.sort(typedSources, createTypeComparator());
	}

	public static Comparator createTypeComparator() {
		return new Comparator(){
			public int compare(Object arg0, Object arg1) {
				return ((TypedSource)arg0).getType() - ((TypedSource)arg1).getType();
			}
		};
	}
	public static TypedSource[] createTypedSources(IJavaScriptElement[] javaElements) throws CoreException {
		//Map<IJavaScriptUnit, List<IJavaScriptElement>>
		Map grouped= ReorgUtils.groupByCompilationUnit(Arrays.asList(javaElements));
		List result= new ArrayList(javaElements.length);
		for (Iterator iter= grouped.keySet().iterator(); iter.hasNext();) {
			IJavaScriptUnit cu= (IJavaScriptUnit) iter.next();
			for (Iterator iterator= ((List) grouped.get(cu)).iterator(); iterator.hasNext();) {
				SourceTuple tuple= new SourceTuple(cu);
				TypedSource[] ts= createTypedSources((IJavaScriptElement) iterator.next(), tuple);
				if (ts != null)
					result.addAll(Arrays.asList(ts));				
			}
		}
		return (TypedSource[]) result.toArray(new TypedSource[result.size()]);		
	}

	private static TypedSource[] createTypedSources(IJavaScriptElement elem, SourceTuple tuple) throws CoreException {
		if (! ReorgUtils.isInsideCompilationUnit(elem))
			return null;
		if (elem.getElementType() == IJavaScriptElement.REQUIRE_CONTAINER) 
			return createTypedSourcesForImportContainer(tuple, (IRequireContainer)elem);
		else if (elem.getElementType() == IJavaScriptElement.FIELD) 
			return new TypedSource[] {create(getFieldSource((IField)elem, tuple), elem.getElementType())};
		return new TypedSource[] {create(getSourceOfDeclararationNode(elem, tuple.unit), elem.getElementType())};
	}

	private static TypedSource[] createTypedSourcesForImportContainer(SourceTuple tuple, IRequireContainer container) throws JavaScriptModelException, CoreException {
		IJavaScriptElement[] imports= container.getChildren();
		List result= new ArrayList(imports.length);
		for (int i= 0; i < imports.length; i++) {
			result.addAll(Arrays.asList(createTypedSources(imports[i], tuple)));
		}
		return (TypedSource[]) result.toArray(new TypedSource[result.size()]);
	}

	private static String getFieldSource(IField field, SourceTuple tuple) throws CoreException {
		if (tuple.node == null) {
			ASTParser parser= ASTParser.newParser(AST.JLS3);
			parser.setSource(tuple.unit);
			tuple.node= (JavaScriptUnit) parser.createAST(null);
		}
		FieldDeclaration declaration= ASTNodeSearchUtil.getFieldDeclarationNode(field, tuple.node);
		if (declaration.fragments().size() == 1)
			return getSourceOfDeclararationNode(field, tuple.unit);
		VariableDeclarationFragment declarationFragment= ASTNodeSearchUtil.getFieldDeclarationFragmentNode(field, tuple.node);
		IBuffer buffer= tuple.unit.getBuffer();
		StringBuffer buff= new StringBuffer();
		buff.append(buffer.getText(declaration.getStartPosition(), ((ASTNode) declaration.fragments().get(0)).getStartPosition() - declaration.getStartPosition()));
		buff.append(buffer.getText(declarationFragment.getStartPosition(), declarationFragment.getLength()));
		buff.append(";"); //$NON-NLS-1$
		return buff.toString();
	}

	private static String getSourceOfDeclararationNode(IJavaScriptElement elem, IJavaScriptUnit cu) throws JavaScriptModelException, CoreException {
		Assert.isTrue(elem.getElementType() != IJavaScriptElement.REQUIRE_CONTAINER);
		if (elem instanceof ISourceReference) {
			ISourceReference reference= (ISourceReference) elem;
			String source= reference.getSource();
			if (source != null)
				return Strings.trimIndentation(source, cu.getJavaScriptProject(), false);
		}
		return ""; //$NON-NLS-1$
	}
}
