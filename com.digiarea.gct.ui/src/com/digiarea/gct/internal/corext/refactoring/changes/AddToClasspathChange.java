/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.changes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IIncludePathEntry;
import com.digiarea.gct.core.JavaScriptConventions;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.internal.corext.refactoring.RefactoringCoreMessages;
import com.digiarea.gct.internal.corext.refactoring.base.JDTChange;

public class AddToClasspathChange extends JDTChange {
	
	private IClosureProject fProjectHandle;
	private IIncludePathEntry fEntryToAdd;
	
	public AddToClasspathChange(IClosureProject project, IIncludePathEntry entryToAdd) {
		fProjectHandle= project;
		fEntryToAdd= entryToAdd;
	}
	
	public AddToClasspathChange(IClosureProject project, String sourceFolderName){
		this(project, ClosureCore.newSourceEntry(project.getPath().append(sourceFolderName)));
	}
	
	/**
	 * Adds a new project class path entry to the project.
	 * @param project
	 * @param newProjectEntry (must be absolute <code>IPath</code>)
	 */
	public AddToClasspathChange(IClosureProject project, IPath newProjectEntry){
		this(project, ClosureCore.newProjectEntry(newProjectEntry));
	}
	
	public AddToClasspathChange(IClosureProject project, int entryKind, IPath path, IPath sourceAttachmentPath, IPath sourceAttachmentRootPath){
		this(project, createNewClasspathEntry(entryKind, path, sourceAttachmentPath, sourceAttachmentRootPath));
	}

	public RefactoringStatus isValid(IProgressMonitor pm) throws CoreException {
		// .classpath file will be handled by JDT/Core.
		return super.isValid(pm, READ_ONLY | DIRTY);
	}
	
	public Change perform(IProgressMonitor pm) throws CoreException {
		pm.beginTask(getName(), 1);
		try {
			if (validateClasspath()) {
				getJavaProject().setRawIncludepath(getNewClasspathEntries(), new SubProgressMonitor(pm, 1));
				IPath classpathEntryPath= ClosureCore.getResolvedIncludepathEntry(fEntryToAdd).getPath();
				return new DeleteFromClasspathChange(classpathEntryPath, getJavaProject());
			} else {
				return new NullChange();
			}
		} finally {
			pm.done();
		}		
	}
	
	public boolean validateClasspath() throws JavaScriptModelException {
		IClosureProject javaProject= getJavaProject();
		IIncludePathEntry[] newClasspathEntries= getNewClasspathEntries();
		return JavaScriptConventions.validateClasspath(javaProject, newClasspathEntries,null).isOK();
	}
	
	private IIncludePathEntry[] getNewClasspathEntries() throws JavaScriptModelException{
		IIncludePathEntry[] entries= getJavaProject().getRawIncludepath();
		List cp= new ArrayList(entries.length + 1);
		cp.addAll(Arrays.asList(entries));
		cp.add(fEntryToAdd);
		return (IIncludePathEntry[])cp.toArray(new IIncludePathEntry[cp.size()]);
	}
	
	private static IIncludePathEntry createNewClasspathEntry(int kind, IPath path, IPath sourceAttach, IPath sourceAttachRoot){
		switch(kind){
			case IIncludePathEntry.CPE_LIBRARY:
				return ClosureCore.newLibraryEntry(path, sourceAttach, sourceAttachRoot);
			case IIncludePathEntry.CPE_PROJECT:
				return ClosureCore.newProjectEntry(path);
			case IIncludePathEntry.CPE_SOURCE:
				return ClosureCore.newSourceEntry(path);
			case IIncludePathEntry.CPE_VARIABLE:
				return ClosureCore.newVariableEntry(path, sourceAttach, sourceAttachRoot);	
			case IIncludePathEntry.CPE_CONTAINER:
				return ClosureCore.newContainerEntry(path);	
			default:
				Assert.isTrue(false);
				return null;	
		}
	}
	
	private IClosureProject getJavaProject(){
		return fProjectHandle;
	}

	public String getName() {
		return RefactoringCoreMessages.AddToClasspathChange_add + getJavaProject().getElementName(); 
 
	}

	public Object getModifiedElement() {
		return getJavaProject();
	}
	
	public IIncludePathEntry getClasspathEntry() {
		return fEntryToAdd;
	}
}
