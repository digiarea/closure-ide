/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.structure;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import com.digiarea.gct.core.dom.AST;
import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.ASTVisitor;
import com.digiarea.gct.core.dom.Expression;
import com.digiarea.gct.core.dom.FieldAccess;
import com.digiarea.gct.core.dom.FunctionInvocation;
import com.digiarea.gct.core.dom.FunctionRef;
import com.digiarea.gct.core.dom.IBinding;
import com.digiarea.gct.core.dom.IPackageBinding;
import com.digiarea.gct.core.dom.ITypeBinding;
import com.digiarea.gct.core.dom.MemberRef;
import com.digiarea.gct.core.dom.Name;
import com.digiarea.gct.core.dom.QualifiedName;
import com.digiarea.gct.core.dom.SimpleName;
import com.digiarea.gct.core.dom.Type;
import com.digiarea.gct.internal.corext.dom.ASTFlattener;
import com.digiarea.gct.internal.corext.dom.ASTNodeFactory;
import com.digiarea.gct.internal.corext.dom.ASTNodes;
import com.digiarea.gct.internal.corext.dom.Bindings;
import com.digiarea.gct.internal.corext.refactoring.RefactoringCoreMessages;
import com.digiarea.gct.internal.corext.refactoring.base.JavaStatusContext;

/* package */ class MoveStaticMemberAnalyzer extends ASTVisitor {

	protected RefactoringStatus fStatus;
	
	protected ITypeBinding fSource;
	protected ITypeBinding fTarget;
	protected CompilationUnitRewrite fCuRewrite;
	protected IBinding[] fMembers;

	protected boolean fNeedsImport;

	protected Set fProcessed;
	
	protected static final String REFERENCE_UPDATE= RefactoringCoreMessages.MoveMembersRefactoring_referenceUpdate; 
	
	public MoveStaticMemberAnalyzer(CompilationUnitRewrite cuRewrite, IBinding[] members, ITypeBinding source, ITypeBinding target) {
		super(true);
		fStatus= new RefactoringStatus();
		fCuRewrite= cuRewrite;
		fMembers= members;
		fSource= source;
		fTarget= target;
		fProcessed= new HashSet();
	}
	
	public RefactoringStatus getStatus() {
		return fStatus;
	}
	
	protected boolean isProcessed(ASTNode node) {
		return fProcessed.contains(node);
	}
	
	protected void rewrite(SimpleName node, ITypeBinding type) {
		AST ast= node.getAST();
		Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
		fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
		Name dummy= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
		QualifiedName name= ast.newQualifiedName(dummy, ast.newSimpleName(node.getIdentifier()));
		fCuRewrite.getASTRewrite().replace(node, name, fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
		fCuRewrite.getImportRemover().registerRemovedNode(node);
		fProcessed.add(node);
		fNeedsImport= true;
	}
	
	protected void rewrite(QualifiedName node, ITypeBinding type) {
		rewriteName(node.getQualifier(), type);
		fProcessed.add(node.getName());
	}
	
	protected void rewrite(FieldAccess node, ITypeBinding type) {
		Expression exp= node.getExpression();
		if (exp == null) {
			Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
			fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
			exp= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
			fCuRewrite.getASTRewrite().set(node, FieldAccess.EXPRESSION_PROPERTY, exp,  fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fNeedsImport= true;
		} else if (exp instanceof Name) {
			rewriteName((Name)exp, type);
		} else {
			rewriteExpression(node, exp, type);
		}
		fProcessed.add(node.getName());
	}
	
	protected void rewrite(FunctionInvocation node, ITypeBinding type) {
		Expression exp= node.getExpression();
		if (exp == null) {
			Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
			fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
			exp= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
			fCuRewrite.getASTRewrite().set(node, FunctionInvocation.EXPRESSION_PROPERTY, exp, fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fNeedsImport= true;
		} else if (exp instanceof Name) {
			rewriteName((Name)exp, type);
		} else {
			rewriteExpression(node, exp, type);
		}
		fProcessed.add(node.getName());
	}
	
	protected void rewrite(MemberRef node, ITypeBinding type) {
		Name qualifier= node.getQualifier();
		if (qualifier == null) {
			Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
			fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
			qualifier= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
			fCuRewrite.getASTRewrite().set(node, MemberRef.QUALIFIER_PROPERTY, qualifier, fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fNeedsImport= true;
		} else {
			rewriteName(qualifier, type);
		}
		fProcessed.add(node.getName());
	}
	
	protected void rewrite(FunctionRef node, ITypeBinding type) {
		Name qualifier= node.getQualifier();
		if (qualifier == null) {
			Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
			fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
			qualifier= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
			fCuRewrite.getASTRewrite().set(node, FunctionRef.QUALIFIER_PROPERTY, qualifier, fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fNeedsImport= true;
		} else {
			rewriteName(qualifier, type);
		}
		fProcessed.add(node.getName());
	}

	private void rewriteName(Name name, ITypeBinding type) {
		AST creator= name.getAST();
		boolean fullyQualified= false;
		if (name instanceof QualifiedName) {
			SimpleName left= ASTNodes.getLeftMostSimpleName(name);
			if (left.resolveBinding() instanceof IPackageBinding)
				fullyQualified= true;
		}
		if (fullyQualified) {
			fCuRewrite.getASTRewrite().replace(
				name, 
				ASTNodeFactory.newName(creator, type.getQualifiedName()),
				fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fCuRewrite.getImportRemover().registerRemovedNode(name);
		} else {
			Type result= fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST());
			fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
			Name n= ASTNodeFactory.newName(fCuRewrite.getAST(), ASTFlattener.asString(result));
			fCuRewrite.getASTRewrite().replace(
				name, 
				n,
				fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
			fCuRewrite.getImportRemover().registerRemovedNode(name);
			fNeedsImport= true;
		}
	}
			
	private void rewriteExpression(ASTNode node, Expression exp, ITypeBinding type) {
		fCuRewrite.getASTRewrite().replace(exp, fCuRewrite.getImportRewrite().addImport(type, fCuRewrite.getAST()), fCuRewrite.createGroupDescription(REFERENCE_UPDATE));
		fCuRewrite.getImportRemover().registerAddedImport(type.getQualifiedName());
		fCuRewrite.getImportRemover().registerRemovedNode(exp);
		fNeedsImport= true;
		nonStaticAccess(node);
	}
	
	protected void nonStaticAccess(ASTNode node) {
		fStatus.addWarning(RefactoringCoreMessages.MoveStaticMemberAnalyzer_nonStatic,  
			JavaStatusContext.create(fCuRewrite.getCu(), node));
	}
	
	protected boolean isStaticAccess(Expression exp, ITypeBinding type) {
		if (!(exp instanceof Name))
			return false;
		return Bindings.equals(type, ((Name)exp).resolveBinding());
	} 
	
	protected boolean isMovedMember(IBinding binding) {
		if (binding == null)
			return false;
		for (int i= 0; i < fMembers.length; i++) {
			if (Bindings.equals(fMembers[i], binding))
				return true;
		}
		return false;
	}
}
