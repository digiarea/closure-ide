/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.typeconstraints;

import com.digiarea.gct.core.dom.AnonymousClassDeclaration;
import com.digiarea.gct.core.dom.ArrayAccess;
import com.digiarea.gct.core.dom.ArrayCreation;
import com.digiarea.gct.core.dom.ArrayInitializer;
import com.digiarea.gct.core.dom.ArrayType;
import com.digiarea.gct.core.dom.Assignment;
import com.digiarea.gct.core.dom.Block;
import com.digiarea.gct.core.dom.BooleanLiteral;
import com.digiarea.gct.core.dom.BreakStatement;
import com.digiarea.gct.core.dom.CatchClause;
import com.digiarea.gct.core.dom.CharacterLiteral;
import com.digiarea.gct.core.dom.ClassInstanceCreation;
import com.digiarea.gct.core.dom.ConditionalExpression;
import com.digiarea.gct.core.dom.ConstructorInvocation;
import com.digiarea.gct.core.dom.ContinueStatement;
import com.digiarea.gct.core.dom.DoStatement;
import com.digiarea.gct.core.dom.EmptyStatement;
import com.digiarea.gct.core.dom.ExpressionStatement;
import com.digiarea.gct.core.dom.FieldAccess;
import com.digiarea.gct.core.dom.FieldDeclaration;
import com.digiarea.gct.core.dom.ForInStatement;
import com.digiarea.gct.core.dom.ForStatement;
import com.digiarea.gct.core.dom.FunctionDeclaration;
import com.digiarea.gct.core.dom.FunctionExpression;
import com.digiarea.gct.core.dom.FunctionInvocation;
import com.digiarea.gct.core.dom.IfStatement;
import com.digiarea.gct.core.dom.ImportDeclaration;
import com.digiarea.gct.core.dom.InfixExpression;
import com.digiarea.gct.core.dom.Initializer;
import com.digiarea.gct.core.dom.InstanceofExpression;
import com.digiarea.gct.core.dom.JSdoc;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.LabeledStatement;
import com.digiarea.gct.core.dom.ListExpression;
import com.digiarea.gct.core.dom.NullLiteral;
import com.digiarea.gct.core.dom.NumberLiteral;
import com.digiarea.gct.core.dom.ObjectLiteral;
import com.digiarea.gct.core.dom.ObjectLiteralField;
import com.digiarea.gct.core.dom.PackageDeclaration;
import com.digiarea.gct.core.dom.ParenthesizedExpression;
import com.digiarea.gct.core.dom.PostfixExpression;
import com.digiarea.gct.core.dom.PrefixExpression;
import com.digiarea.gct.core.dom.PrimitiveType;
import com.digiarea.gct.core.dom.QualifiedName;
import com.digiarea.gct.core.dom.RegularExpressionLiteral;
import com.digiarea.gct.core.dom.ReturnStatement;
import com.digiarea.gct.core.dom.SimpleName;
import com.digiarea.gct.core.dom.SimpleType;
import com.digiarea.gct.core.dom.SingleVariableDeclaration;
import com.digiarea.gct.core.dom.StringLiteral;
import com.digiarea.gct.core.dom.SuperConstructorInvocation;
import com.digiarea.gct.core.dom.SuperFieldAccess;
import com.digiarea.gct.core.dom.SuperMethodInvocation;
import com.digiarea.gct.core.dom.SwitchCase;
import com.digiarea.gct.core.dom.SwitchStatement;
import com.digiarea.gct.core.dom.ThisExpression;
import com.digiarea.gct.core.dom.ThrowStatement;
import com.digiarea.gct.core.dom.TryStatement;
import com.digiarea.gct.core.dom.TypeDeclaration;
import com.digiarea.gct.core.dom.TypeDeclarationStatement;
import com.digiarea.gct.core.dom.TypeLiteral;
import com.digiarea.gct.core.dom.UndefinedLiteral;
import com.digiarea.gct.core.dom.VariableDeclarationExpression;
import com.digiarea.gct.core.dom.VariableDeclarationFragment;
import com.digiarea.gct.core.dom.VariableDeclarationStatement;
import com.digiarea.gct.core.dom.WhileStatement;
import com.digiarea.gct.core.dom.WithStatement;

/**
 * Empty implementation of a creator - provided to allow subclasses to override only a subset of methods.
 * Subclass to provide constraint creation functionality.
 */
public class ConstraintCreator {

	public static final ITypeConstraint[] EMPTY_ARRAY= new ITypeConstraint[0];
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.AnonymousClassDeclaration)
	 */
	public ITypeConstraint[] create(AnonymousClassDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayAccess)
	 */
	public ITypeConstraint[] create(ArrayAccess node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayCreation)
	 */
	public ITypeConstraint[] create(ArrayCreation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayInitializer)
	 */
	public ITypeConstraint[] create(ArrayInitializer node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ArrayType)
	 */
	public ITypeConstraint[] create(ArrayType node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Assignment)
	 */
	public ITypeConstraint[] create(Assignment node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Block)
	 */
	public ITypeConstraint[] create(Block node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.BooleanLiteral)
	 */
	public ITypeConstraint[] create(BooleanLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.BreakStatement)
	 */
	public ITypeConstraint[] create(BreakStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.CatchClause)
	 */
	public ITypeConstraint[] create(CatchClause node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.CharacterLiteral)
	 */
	public ITypeConstraint[] create(CharacterLiteral node) {
		return EMPTY_ARRAY;
	}

	public ITypeConstraint[] create(RegularExpressionLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ClassInstanceCreation)
	 */
	public ITypeConstraint[] create(ClassInstanceCreation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.JavaScriptUnit)
	 */
	public ITypeConstraint[] create(JavaScriptUnit node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ConditionalExpression)
	 */
	public ITypeConstraint[] create(ConditionalExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ConstructorInvocation)
	 */
	public ITypeConstraint[] create(ConstructorInvocation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ContinueStatement)
	 */
	public ITypeConstraint[] create(ContinueStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.DoStatement)
	 */
	public ITypeConstraint[] create(DoStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.EmptyStatement)
	 */
	public ITypeConstraint[] create(EmptyStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ExpressionStatement)
	 */
	public ITypeConstraint[] create(ExpressionStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FieldAccess)
	 */
	public ITypeConstraint[] create(FieldAccess node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FieldDeclaration)
	 */
	public ITypeConstraint[] create(FieldDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ForStatement)
	 */
	public ITypeConstraint[] create(ForStatement node) {
		return EMPTY_ARRAY;
	}
	public ITypeConstraint[] create(ForInStatement node) {
		return EMPTY_ARRAY;
	}
	public ITypeConstraint[] create(FunctionExpression node) {
		return EMPTY_ARRAY;
	}
	public ITypeConstraint[] create(ObjectLiteral node) {
		return EMPTY_ARRAY;
	}
	public ITypeConstraint[] create(ObjectLiteralField node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.IfStatement)
	 */
	public ITypeConstraint[] create(IfStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ImportDeclaration)
	 */
	public ITypeConstraint[] create(ImportDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.InfixExpression)
	 */
	public ITypeConstraint[] create(InfixExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Initializer)
	 */
	public ITypeConstraint[] create(Initializer node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.InstanceofExpression)
	 */
	public ITypeConstraint[] create(InstanceofExpression node) {
		return EMPTY_ARRAY;
	}

	public ITypeConstraint[] create(ListExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.Javadoc)
	 */
	public ITypeConstraint[] create(JSdoc node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.LabeledStatement)
	 */
	public ITypeConstraint[] create(LabeledStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FunctionDeclaration)
	 */
	public ITypeConstraint[] create(FunctionDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.FunctionInvocation)
	 */
	public ITypeConstraint[] create(FunctionInvocation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.NullLiteral)
	 */
	public ITypeConstraint[] create(NullLiteral node) {
		return EMPTY_ARRAY;
	}

	public ITypeConstraint[] create(UndefinedLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.NumberLiteral)
	 */
	public ITypeConstraint[] create(NumberLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PackageDeclaration)
	 */
	public ITypeConstraint[] create(PackageDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ParenthesizedExpression)
	 */
	public ITypeConstraint[] create(ParenthesizedExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PostfixExpression)
	 */
	public ITypeConstraint[] create(PostfixExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PrefixExpression)
	 */
	public ITypeConstraint[] create(PrefixExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.PrimitiveType)
	 */
	public ITypeConstraint[] create(PrimitiveType node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.QualifiedName)
	 */
	public ITypeConstraint[] create(QualifiedName node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ReturnStatement)
	 */
	public ITypeConstraint[] create(ReturnStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SimpleName)
	 */
	public ITypeConstraint[] create(SimpleName node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SimpleType)
	 */
	public ITypeConstraint[] create(SimpleType node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SingleVariableDeclaration)
	 */
	public ITypeConstraint[] create(SingleVariableDeclaration node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.StringLiteral)
	 */
	public ITypeConstraint[] create(StringLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperConstructorInvocation)
	 */
	public ITypeConstraint[] create(SuperConstructorInvocation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperFieldAccess)
	 */
	public ITypeConstraint[] create(SuperFieldAccess node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SuperMethodInvocation)
	 */
	public ITypeConstraint[] create(SuperMethodInvocation node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SwitchCase)
	 */
	public ITypeConstraint[] create(SwitchCase node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.SwitchStatement)
	 */
	public ITypeConstraint[] create(SwitchStatement node) {
		return EMPTY_ARRAY;
	}


	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ThisExpression)
	 */
	public ITypeConstraint[] create(ThisExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.ThrowStatement)
	 */
	public ITypeConstraint[] create(ThrowStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TryStatement)
	 */
	public ITypeConstraint[] create(TryStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeDeclaration)
	 */
	public ITypeConstraint[] create(TypeDeclaration node) {
		return EMPTY_ARRAY;
		
		// TODO account for enums and annotations
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeDeclarationStatement)
	 */
	public ITypeConstraint[] create(TypeDeclarationStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.TypeLiteral)
	 */
	public ITypeConstraint[] create(TypeLiteral node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationExpression)
	 */
	public ITypeConstraint[] create(VariableDeclarationExpression node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationFragment)
	 */
	public ITypeConstraint[] create(VariableDeclarationFragment node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.VariableDeclarationStatement)
	 */
	public ITypeConstraint[] create(VariableDeclarationStatement node) {
		return EMPTY_ARRAY;
	}

	/* (non-Javadoc)
	 * @see com.digiarea.gct.core.dom.ASTVisitor#visit(com.digiarea.gct.core.dom.WhileStatement)
	 */
	public ITypeConstraint[] create(WhileStatement node) {
		return EMPTY_ARRAY;
	}

	public ITypeConstraint[] create(WithStatement node) {
		return EMPTY_ARRAY;
	}

}
