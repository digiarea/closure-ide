/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.code;

import java.util.ArrayList;
import java.util.Collection;

import com.digiarea.gct.core.dom.ASTMatcher;
import com.digiarea.gct.core.dom.ASTNode;
import com.digiarea.gct.core.dom.ASTVisitor;
import com.digiarea.gct.core.dom.AnonymousClassDeclaration;
import com.digiarea.gct.core.dom.ArrayAccess;
import com.digiarea.gct.core.dom.ArrayCreation;
import com.digiarea.gct.core.dom.ArrayInitializer;
import com.digiarea.gct.core.dom.ArrayType;
import com.digiarea.gct.core.dom.Assignment;
import com.digiarea.gct.core.dom.Block;
import com.digiarea.gct.core.dom.BlockComment;
import com.digiarea.gct.core.dom.BooleanLiteral;
import com.digiarea.gct.core.dom.BreakStatement;
import com.digiarea.gct.core.dom.CatchClause;
import com.digiarea.gct.core.dom.CharacterLiteral;
import com.digiarea.gct.core.dom.ClassInstanceCreation;
import com.digiarea.gct.core.dom.ConditionalExpression;
import com.digiarea.gct.core.dom.ConstructorInvocation;
import com.digiarea.gct.core.dom.ContinueStatement;
import com.digiarea.gct.core.dom.DoStatement;
import com.digiarea.gct.core.dom.EmptyStatement;
import com.digiarea.gct.core.dom.EnhancedForStatement;
import com.digiarea.gct.core.dom.ExpressionStatement;
import com.digiarea.gct.core.dom.FieldAccess;
import com.digiarea.gct.core.dom.FieldDeclaration;
import com.digiarea.gct.core.dom.ForInStatement;
import com.digiarea.gct.core.dom.ForStatement;
import com.digiarea.gct.core.dom.FunctionDeclaration;
import com.digiarea.gct.core.dom.FunctionExpression;
import com.digiarea.gct.core.dom.FunctionInvocation;
import com.digiarea.gct.core.dom.FunctionRef;
import com.digiarea.gct.core.dom.FunctionRefParameter;
import com.digiarea.gct.core.dom.IfStatement;
import com.digiarea.gct.core.dom.ImportDeclaration;
import com.digiarea.gct.core.dom.InfixExpression;
import com.digiarea.gct.core.dom.Initializer;
import com.digiarea.gct.core.dom.InstanceofExpression;
import com.digiarea.gct.core.dom.JSdoc;
import com.digiarea.gct.core.dom.JavaScriptUnit;
import com.digiarea.gct.core.dom.LabeledStatement;
import com.digiarea.gct.core.dom.LineComment;
import com.digiarea.gct.core.dom.ListExpression;
import com.digiarea.gct.core.dom.MemberRef;
import com.digiarea.gct.core.dom.Modifier;
import com.digiarea.gct.core.dom.NullLiteral;
import com.digiarea.gct.core.dom.NumberLiteral;
import com.digiarea.gct.core.dom.ObjectLiteral;
import com.digiarea.gct.core.dom.ObjectLiteralField;
import com.digiarea.gct.core.dom.PackageDeclaration;
import com.digiarea.gct.core.dom.ParenthesizedExpression;
import com.digiarea.gct.core.dom.PostfixExpression;
import com.digiarea.gct.core.dom.PrefixExpression;
import com.digiarea.gct.core.dom.PrimitiveType;
import com.digiarea.gct.core.dom.QualifiedName;
import com.digiarea.gct.core.dom.QualifiedType;
import com.digiarea.gct.core.dom.RegularExpressionLiteral;
import com.digiarea.gct.core.dom.ReturnStatement;
import com.digiarea.gct.core.dom.SimpleName;
import com.digiarea.gct.core.dom.SimpleType;
import com.digiarea.gct.core.dom.SingleVariableDeclaration;
import com.digiarea.gct.core.dom.StringLiteral;
import com.digiarea.gct.core.dom.SuperConstructorInvocation;
import com.digiarea.gct.core.dom.SuperFieldAccess;
import com.digiarea.gct.core.dom.SuperMethodInvocation;
import com.digiarea.gct.core.dom.SwitchCase;
import com.digiarea.gct.core.dom.SwitchStatement;
import com.digiarea.gct.core.dom.TagElement;
import com.digiarea.gct.core.dom.TextElement;
import com.digiarea.gct.core.dom.ThisExpression;
import com.digiarea.gct.core.dom.ThrowStatement;
import com.digiarea.gct.core.dom.TryStatement;
import com.digiarea.gct.core.dom.TypeDeclaration;
import com.digiarea.gct.core.dom.TypeDeclarationStatement;
import com.digiarea.gct.core.dom.TypeLiteral;
import com.digiarea.gct.core.dom.UndefinedLiteral;
import com.digiarea.gct.core.dom.VariableDeclarationExpression;
import com.digiarea.gct.core.dom.VariableDeclarationFragment;
import com.digiarea.gct.core.dom.VariableDeclarationStatement;
import com.digiarea.gct.core.dom.WhileStatement;
import com.digiarea.gct.core.dom.WithStatement;
import com.digiarea.gct.internal.corext.dom.JdtASTMatcher;

class AstMatchingNodeFinder {
	
	private AstMatchingNodeFinder(){
	}
	
	public static ASTNode[] findMatchingNodes(ASTNode scope, ASTNode node){
		Visitor visitor= new Visitor(node);
		scope.accept(visitor);
		return visitor.getMatchingNodes();
	}
	
	private static class Visitor extends ASTVisitor{
		
		Collection fFound;
		ASTMatcher fMatcher;
		ASTNode fNodeToMatch;
		
		Visitor(ASTNode nodeToMatch){
			fNodeToMatch= nodeToMatch;
			fFound= new ArrayList();
			fMatcher= new JdtASTMatcher();
		}
		
		ASTNode[] getMatchingNodes(){
			return (ASTNode[]) fFound.toArray(new ASTNode[fFound.size()]);
		}
		
		private boolean matches(ASTNode node){
			fFound.add(node);
			return false;
		}
		
		public boolean visit(AnonymousClassDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ArrayAccess node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ArrayCreation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ArrayInitializer node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ArrayType node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(Assignment node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(Block node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(BooleanLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(BreakStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FunctionExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ObjectLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ObjectLiteralField node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}
 
		public boolean visit(CatchClause node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(CharacterLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}
		public boolean visit(RegularExpressionLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ClassInstanceCreation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(JavaScriptUnit node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ConditionalExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ConstructorInvocation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ContinueStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(DoStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(EmptyStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ExpressionStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FieldAccess node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FieldDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ForStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ForInStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(IfStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ImportDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(InfixExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(Initializer node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

        public boolean visit(InstanceofExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
        }

		public boolean visit(JSdoc node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(LabeledStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}


		public boolean visit(ListExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}
		public boolean visit(FunctionDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FunctionInvocation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}


		public boolean visit(NullLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(UndefinedLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(NumberLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(PackageDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ParenthesizedExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(PostfixExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(PrefixExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(PrimitiveType node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(QualifiedName node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ReturnStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SimpleName node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SimpleType node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SingleVariableDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(StringLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SuperConstructorInvocation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SuperFieldAccess node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SuperMethodInvocation node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SwitchCase node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(SwitchStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}


		public boolean visit(ThisExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(ThrowStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TryStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TypeDeclaration node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TypeDeclarationStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TypeLiteral node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(VariableDeclarationExpression node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(VariableDeclarationFragment node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(VariableDeclarationStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(WhileStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}


		public boolean visit(WithStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(BlockComment node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(EnhancedForStatement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(LineComment node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(MemberRef node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FunctionRef node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(FunctionRefParameter node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(Modifier node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(QualifiedType node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TagElement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}

		public boolean visit(TextElement node) {
			if (node.subtreeMatch(fMatcher, fNodeToMatch))
				return matches(node);
			return super.visit(node);
		}
	}
}
