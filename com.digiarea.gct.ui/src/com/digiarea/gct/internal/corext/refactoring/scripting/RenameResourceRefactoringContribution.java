/*******************************************************************************
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.scripting;

import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;

import com.digiarea.gct.core.refactoring.IJavaScriptRefactorings;
import com.digiarea.gct.core.refactoring.descriptors.RenameJavaScriptElementDescriptor;
import com.digiarea.gct.internal.corext.refactoring.JDTRefactoringContribution;
import com.digiarea.gct.internal.corext.refactoring.rename.JavaRenameRefactoring;
import com.digiarea.gct.internal.corext.refactoring.rename.RenameResourceProcessor;

/**
 * Refactoring contribution for the rename resource refactoring.
 * 
 * 
 */
public final class RenameResourceRefactoringContribution extends JDTRefactoringContribution {

	/**
	 * {@inheritDoc}
	 */
	public Refactoring createRefactoring(final RefactoringDescriptor descriptor) {
		return new JavaRenameRefactoring(new RenameResourceProcessor(null));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public RefactoringDescriptor createDescriptor() {
		return new RenameJavaScriptElementDescriptor(IJavaScriptRefactorings.RENAME_RESOURCE);
	}
}
