/*******************************************************************************
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.refactoring.scripting;

import java.util.Map;

import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;

import com.digiarea.gct.core.IFunction;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.refactoring.IJavaScriptRefactorings;
import com.digiarea.gct.core.refactoring.descriptors.RenameJavaScriptElementDescriptor;
import com.digiarea.gct.internal.corext.refactoring.JDTRefactoringContribution;
import com.digiarea.gct.internal.corext.refactoring.JDTRefactoringDescriptor;
import com.digiarea.gct.internal.corext.refactoring.rename.JavaRenameProcessor;
import com.digiarea.gct.internal.corext.refactoring.rename.JavaRenameRefactoring;
import com.digiarea.gct.internal.corext.refactoring.rename.MethodChecks;
import com.digiarea.gct.internal.corext.refactoring.rename.RenameNonVirtualMethodProcessor;
import com.digiarea.gct.internal.corext.refactoring.rename.RenameVirtualMethodProcessor;

/**
 * Refactoring contribution for the rename method refactoring.
 * 
 * 
 */
public final class RenameMethodRefactoringContribution extends JDTRefactoringContribution {

	/**
	 * {@inheritDoc}
	 */
	public Refactoring createRefactoring(final RefactoringDescriptor descriptor) throws JavaScriptModelException {
		String project= descriptor.getProject();
		Map arguments= ((JDTRefactoringDescriptor) descriptor).getArguments();
		String input= (String) arguments.get(JDTRefactoringDescriptor.ATTRIBUTE_INPUT);
		IFunction method= (IFunction) JDTRefactoringDescriptor.handleToElement(project, input);
		
		JavaRenameProcessor processor;
		if (MethodChecks.isVirtual(method)) {
			processor= new RenameVirtualMethodProcessor(method);
		} else {
			processor= new RenameNonVirtualMethodProcessor(method);
		}
		return new JavaRenameRefactoring(processor);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public RefactoringDescriptor createDescriptor() {
		return new RenameJavaScriptElementDescriptor(IJavaScriptRefactorings.RENAME_METHOD);
	}
}
