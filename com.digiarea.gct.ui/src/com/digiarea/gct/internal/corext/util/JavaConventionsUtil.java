/*******************************************************************************
 * Copyright (c) 2007, 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.corext.util;

import org.eclipse.core.runtime.IStatus;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.JavaScriptConventions;

/**
 * Provides methods for checking Java-specific conventions such as name
 * syntax.
 * <p>
 * This is a wrapper for {@link JavaScriptConventions} with more convenient
 * <code>validate*(..)</code> methods. If multiple validations are planned on
 * the same element, please use {@link JavaScriptConventions} directly (with
 * the arguments from {@link #getSourceComplianceLevels(IJavaScriptElement)}).
 * </p>
 * 
 * @see JDTUIHelperClasses
 */
public class JavaConventionsUtil {

	/**
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return a <code>String[]</code> whose <code>[0]</code> is the
	 *         {@link ClosureCore#COMPILER_SOURCE} and whose <code>[1]</code>
	 *         is the {@link ClosureCore#COMPILER_COMPLIANCE} level at the
	 *         given <code>context</code>.
	 */
	public static String[] getSourceComplianceLevels(IJavaScriptElement context) {
		if (context != null) {
			IClosureProject javaProject = context.getJavaScriptProject();
			if (javaProject != null) {
				return new String[]{javaProject.getOption(ClosureCore.COMPILER_SOURCE, true), javaProject.getOption(ClosureCore.COMPILER_COMPLIANCE, true)};
			}
		}
		return new String[]{ClosureCore.getOption(ClosureCore.COMPILER_SOURCE), ClosureCore.getOption(ClosureCore.COMPILER_COMPLIANCE)};
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateCompilationUnitName(String, String,
	 *      String)
	 */
	public static IStatus validateCompilationUnitName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateCompilationUnitName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateClassFileName(String, String,
	 *      String)
	 */
	public static IStatus validateClassFileName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateClassFileName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateFieldName(String, String, String)
	 */
	public static IStatus validateFieldName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateFieldName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateIdentifier(String, String, String)
	 */
	public static IStatus validateIdentifier(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateIdentifier(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateImportDeclaration(String, String,
	 *      String)
	 */
	public static IStatus validateImportDeclaration(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateImportDeclaration(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateJavaTypeName(String, String, String)
	 */
	public static IStatus validateJavaTypeName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateJavaScriptTypeName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateMethodName(String, String, String)
	 */
	public static IStatus validateMethodName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateFunctionName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validatePackageName(String, String, String)
	 */
	public static IStatus validatePackageName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validatePackageName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}

	/**
	 * @param name
	 *            the name to validate
	 * @param context
	 *            an {@link IJavaScriptElement} or <code>null</code>
	 * @return validation status in <code>context</code>'s project or in the
	 *         workspace
	 * 
	 * @see JavaScriptConventions#validateTypeVariableName(String, String,
	 *      String)
	 */
	public static IStatus validateTypeVariableName(String name, IJavaScriptElement context) {
		String[] sourceComplianceLevels = getSourceComplianceLevels(context);
		return JavaScriptConventions.validateTypeVariableName(name, sourceComplianceLevels[0], sourceComplianceLevels[1]);
	}
}
