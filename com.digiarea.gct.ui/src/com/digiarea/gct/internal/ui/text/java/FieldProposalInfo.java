/*******************************************************************************
 * Copyright (c) 2005, 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.text.java;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;

import com.digiarea.gct.core.CompletionProposal;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IField;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.ILocalVariable;
import com.digiarea.gct.core.IMember;
import com.digiarea.gct.core.IType;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.core.search.IJavaScriptSearchScope;
import com.digiarea.gct.core.search.SearchEngine;
import com.digiarea.gct.core.search.SearchMatch;
import com.digiarea.gct.core.search.SearchParticipant;
import com.digiarea.gct.core.search.SearchPattern;
import com.digiarea.gct.core.search.SearchRequestor;
import com.digiarea.gct.internal.core.DefaultWorkingCopyOwner;
import com.digiarea.gct.internal.core.search.matching.FieldPattern;
import com.digiarea.gct.internal.core.util.QualificationHelpers;
import com.digiarea.gct.internal.corext.template.java.SignatureUtil;
import com.digiarea.gct.internal.ui.Logger;


/**
 * Proposal info that computes the javadoc lazily when it is queried.
 *
 * 
 */
public final class FieldProposalInfo extends MemberProposalInfo {	
	/**
	 * Creates a new proposal info.
	 *
	 * @param project the java project to reference when resolving types
	 * @param proposal the proposal to generate information for
	 */
	public FieldProposalInfo(IClosureProject project, CompletionProposal proposal) {
		super(project, proposal);
	}
	
	/**
	 * <p>Returns the java element that this computer corresponds to, possibly <code>null</code>.</p>
	 * 
	 * <p><b>NOTE:</b> This overrides the parent implementation so that {@link #resolveMember()} is not called
	 * because the field proposal can resolve to a none {@link IMember}.</p>
	 * 
	 * @return the java element that this computer corresponds to, possibly <code>null</code>
	 * @throws JavaScriptModelException
	 */
	public IJavaScriptElement getJavaElement() throws JavaScriptModelException {
		if (!fJavaElementResolved) {
			fJavaElementResolved= true;
			//call the internal resolve rather then #resolveMember
			fElement = resolve();
		}
		return fElement;
	}
	
	/**
	 * @see com.digiarea.gct.internal.ui.text.java.MemberProposalInfo#resolveMember()
	 */
	protected IMember resolveMember() throws JavaScriptModelException {
		IMember member = null;
		IJavaScriptElement element = this.resolve();
		if(element instanceof IMember) {
			member = (IMember)element;
		}
		return member;
	}

	/**
	 * @return {@link IJavaScriptElement} that this field proposal resolves to
	 * 
	 * @throws JavaScriptModelException
	 */
	private IJavaScriptElement resolve() throws JavaScriptModelException {
		//get the type name
		char[] typeNameChars = fProposal.getDeclarationTypeName();
		String declaringTypeName = null;
		if(typeNameChars != null) {
			declaringTypeName = String.valueOf(typeNameChars);
		}
		
		/* try using the signature if type name not set
		 * NOTE: old way of doing things, should be removed at some point
		 */
		if(declaringTypeName == null) {
			char[] declarationSignature= fProposal.getDeclarationSignature();
			if(declarationSignature != null) {
				declaringTypeName = SignatureUtil.stripSignatureToFQN(String.valueOf(declarationSignature));
			}
		}
		
		//find the field
		IJavaScriptElement resolvedField = null;
		if(declaringTypeName != null) {
			String fieldName = String.valueOf(fProposal.getName());
			
			IType[] types = this.fJavaProject.findTypes(declaringTypeName);
			if(types != null && types.length > 0) {
				for(int i = 0; i < types.length && resolvedField == null; ++i) {
					IType type = types[i];
					if (type != null) {
						IField field = type.getField(fieldName);
						if (field.exists()) {
							resolvedField = field;
						}
					}
				}
			} else {
				//create the search pattern
				char[][] seperatedDeclaringTypename = QualificationHelpers.seperateFullyQualifedName(declaringTypeName.toCharArray());
//				if(!CharOperation.equals(seperatedDeclaringTypename[QualificationHelpers.SIMPLE_NAMES_INDEX], IIndexConstants.GLOBAL_SYMBOL)) {
					FieldPattern fieldPattern = new FieldPattern(true, false, false,
								fieldName.toCharArray(),
								seperatedDeclaringTypename[QualificationHelpers.QULIFIERS_INDEX],
								seperatedDeclaringTypename[QualificationHelpers.SIMPLE_NAMES_INDEX],
								SearchPattern.R_EXACT_MATCH);
					
					//search the index for a match
					SearchEngine searchEngine = new SearchEngine(DefaultWorkingCopyOwner.PRIMARY);
					IJavaScriptSearchScope scope = SearchEngine.createJavaSearchScope(new IJavaScriptElement[] {this.fJavaProject});
					final List matches = new ArrayList();
					try {
						searchEngine.search(fieldPattern,
								new SearchParticipant[] {SearchEngine.getDefaultSearchParticipant()},
								scope,
								new SearchRequestor() {
									public void acceptSearchMatch(SearchMatch match) throws CoreException {
										Object element = match.getElement();
										if(element instanceof IField || element instanceof ILocalVariable) {
											matches.add(element);
										}
									}
								},
								new NullProgressMonitor());  //using a NPM here maybe a bad idea, but nothing better to do right now
					}
					catch (CoreException e) {
						Logger.logException("Failed index search for field: " + fieldName, e); //$NON-NLS-1$
					}
					
					// just use the first match found
					if(!matches.isEmpty()) {
						resolvedField = (IJavaScriptElement)matches.get(0);
					}
//				}
			}
		}
		
		return resolvedField;
	}
}
