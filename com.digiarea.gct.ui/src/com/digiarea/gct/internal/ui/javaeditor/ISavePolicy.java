/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.javaeditor;



import com.digiarea.gct.core.IJavaScriptUnit;

public interface ISavePolicy {

	/**
	 *
	 */
	void preSave(IJavaScriptUnit unit);

	/**
	 * Returns the compilation unit in which the argument
	 * has been changed. If the argument is not changed, the
	 * returned result is <code>null</code>.
	 */
	IJavaScriptUnit postSave(IJavaScriptUnit unit);
}
