/*******************************************************************************
 * Licensed Materials - Property of IBM
 * � Copyright IBM Corporation 2014. All Rights Reserved.
 * U.S. Government Users Restricted Rights - Use, duplication or disclosure
 * restricted by GSA ADP Schedule Contract with IBM Corp. 
 *******************************************************************************/

package com.digiarea.gct.internal.ui.preferences;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import com.digiarea.gct.internal.ui.wizards.buildpaths.CPUserLibraryElement;

class NonClosureLibraryFilter extends ViewerFilter {

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof CPUserLibraryElement) {
			CPUserLibraryElement libraryElement = (CPUserLibraryElement) element;
			if (!libraryElement.isClosure()) {
				return true;
			}
			else {
				return false;
			}
		}
		return true;
	}

}