/*******************************************************************************
 * Licensed Materials - Property of IBM
 * � Copyright IBM Corporation 2014. All Rights Reserved.
 * U.S. Government Users Restricted Rights - Use, duplication or disclosure
 * restricted by GSA ADP Schedule Contract with IBM Corp. 
 *******************************************************************************/

package com.digiarea.gct.internal.ui.navigator;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import com.digiarea.gct.core.IClosureProject;

/**
 * Filters non-Closure projects
 */
public class NonClosureProjectsFilter extends ViewerFilter {

	/*
	 * @see ViewerFilter
	 */
	@Override
	public boolean select(Viewer viewer, Object parent, Object element) {
		if (element instanceof IClosureProject)
			return true;
		else if (element instanceof IProject)
			return !((IProject) element).isOpen();

		return true;
	}
}
