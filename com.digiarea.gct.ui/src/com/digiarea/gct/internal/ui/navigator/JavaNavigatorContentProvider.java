/*******************************************************************************
 * Copyright (c) 2003, 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.navigator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.IExtensionStateModel;
import org.eclipse.ui.navigator.IPipelinedTreeContentProvider;
import org.eclipse.ui.navigator.PipelinedShapeModification;
import org.eclipse.ui.navigator.PipelinedViewerUpdate;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IJavaScriptModel;
import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.internal.ui.JavaScriptPlugin;
import com.digiarea.gct.internal.ui.navigator.IExtensionStateConstants.Values;
import com.digiarea.gct.internal.ui.packageview.PackageExplorerContentProvider;
import com.digiarea.gct.ui.PreferenceConstants;

public class JavaNavigatorContentProvider extends PackageExplorerContentProvider implements IPipelinedTreeContentProvider {

	public JavaNavigatorContentProvider() {
		super(false);
	}

	public JavaNavigatorContentProvider(boolean provideMembers) {
		super(provideMembers);
	}

	public static final String JSDT_EXTENSION_ID = "com.digiarea.gct.ui.javaContent"; //$NON-NLS-1$ 
	/**
	 * @deprecated
	 */
	public static final String JDT_EXTENSION_ID = JSDT_EXTENSION_ID;

	private IExtensionStateModel fStateModel;

	private Object fRealInput;

	private IPropertyChangeListener fLayoutPropertyListener;

	public void init(ICommonContentExtensionSite commonContentExtensionSite) {
		IExtensionStateModel stateModel = commonContentExtensionSite.getExtensionStateModel();
		IMemento memento = commonContentExtensionSite.getMemento();

		fStateModel = stateModel;
		restoreState(memento);
		fLayoutPropertyListener = new IPropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				if (Values.IS_LAYOUT_FLAT.equals(event.getProperty())) {
					if (event.getNewValue() != null) {
						boolean newValue = ((Boolean) event.getNewValue()).booleanValue() ? true : false;
						setIsFlatLayout(newValue);
					}
				}

			}
		};
		fStateModel.addPropertyChangeListener(fLayoutPropertyListener);

		IPreferenceStore store = PreferenceConstants.getPreferenceStore();
		boolean showCUChildren = store.getBoolean(PreferenceConstants.SHOW_CU_CHILDREN);
		setProvideMembers(showCUChildren);
		
		boolean isCurrentLayoutFlat = false;
		Integer state= new Integer(store.getInt(JavaNavigatorViewActionProvider.TAG_LAYOUT));

		if (state.intValue() == JavaNavigatorViewActionProvider.FLAT_LAYOUT)
			isCurrentLayoutFlat= true;
		else
			if (state.intValue() == JavaNavigatorViewActionProvider.HIERARCHICAL_LAYOUT)
				isCurrentLayoutFlat= false;
		
		setIsFlatLayout(isCurrentLayoutFlat);
		setShowLibrariesNode(true);
	}

	public void dispose() {
		if (fStateModel != null) {
			fStateModel.removePropertyChangeListener(fLayoutPropertyListener);
		}
		super.dispose();
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		fRealInput = newInput;
		super.inputChanged(viewer, oldInput, findInputElement(newInput));
	}

	public Object getParent(Object element) {
		// Object parent = null;
		//
		// // can't handle IResources
		// if (!(element instanceof IResource)) {
		// parent= super.getParent(element);
		// if (parent instanceof IJavaScriptModel) {
		// return parent.equals(getViewerInput()) ? fRealInput : parent;
		// }
		// if (parent instanceof IClosureProject) {
		// return ((IClosureProject)parent).getProject();
		// }
		// }
		//
		// return parent;

		Object parent = super.getParent(element);
		if (parent instanceof IJavaScriptModel) {
			return ((IJavaScriptModel) parent).getWorkspace().getRoot();
		}
		if (parent instanceof IClosureProject) {
			return ((IClosureProject) parent).getProject();
		}
		return parent;
	}

	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof IWorkspaceRoot) {
			IWorkspaceRoot root = (IWorkspaceRoot) inputElement;
			// return root.getProjects();
			return filterResourceProjects(root.getProjects());
		}
		else if (inputElement instanceof IJavaScriptModel) {
			// return
			// ((IJavaScriptModel)inputElement).getWorkspace().getRoot().getProjects();
			return filterResourceProjects(((IJavaScriptModel) inputElement).getWorkspace().getRoot().getProjects());
		}
		if (inputElement instanceof IProject) {
			return super.getElements(ClosureCore.create((IProject) inputElement));
		}
		return super.getElements(inputElement);
	}

	private static IProject[] filterResourceProjects(IProject[] projects) {
		List<IProject> filteredProjects = new ArrayList<IProject>(projects.length);
		for (int i = 0; i < projects.length; i++) {
			IProject project = projects[i];
			if (!project.isOpen() || isJavaProject(project))
				filteredProjects.add(project);
		}
		return filteredProjects.toArray(new IProject[filteredProjects.size()]);
	}

	private static boolean isJavaProject(IProject project) {
		try {
			return project.hasNature(ClosureCore.NATURE_ID);
		}
		catch (CoreException e) {
			JavaScriptPlugin.log(e);
		}
		return false;
	}

	public boolean hasChildren(Object element) {
		if (element instanceof IProject) {
			return ((IProject) element).isAccessible();
		}
		if (getProvideMembers() && element instanceof IFile && ClosureCore.isJavaScriptLikeFileName(((IFile) element).getName())) {
			/*
			 * TODO: gives false positives for .js files not on an Include
			 * Path
			 */
			return ClosureCore.create((IFile) element) != null;
		}
		return super.hasChildren(element);
	}

	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IWorkspaceRoot) {
			IWorkspaceRoot root = (IWorkspaceRoot) parentElement;
			// return root.getProjects();
			return filterResourceProjects(root.getProjects());
		}
		if (parentElement instanceof IProject) {
			return super.getChildren(ClosureCore.create((IProject) parentElement));
		}
		if (getProvideMembers() && parentElement instanceof IFile && ClosureCore.isJavaScriptLikeFileName(((IFile) parentElement).getName())) {
			return super.getChildren(ClosureCore.create((IFile) parentElement));
		}
		return super.getChildren(parentElement);
	}

	private Object findInputElement(Object newInput) {
		if (newInput instanceof IWorkspaceRoot) {
			return ClosureCore.create((IWorkspaceRoot) newInput);
		}
		return newInput;
	}

	public void restoreState(IMemento memento) {

	}

	public void saveState(IMemento memento) {

	}

	public void getPipelinedChildren(Object parent, Set currentChildren) {
		customize(getChildren(parent), currentChildren);
	}

	public void getPipelinedElements(Object input, Set currentElements) {
		customize(getElements(input), currentElements);
	}

	/**
	 * Adapted from the Common Navigator Content Provider
	 * 
	 * @param javaElements
	 *            the java elements
	 * @param proposedChildren
	 *            the proposed children
	 */
	private void customize(Object[] javaElements, Set<Object> proposedChildren) {
		List<?> elementList = Arrays.asList(javaElements);
		for (Iterator<?> iter = proposedChildren.iterator(); iter.hasNext();) {
			Object element = iter.next();
			IResource resource = null;
			if (element instanceof IResource) {
				resource = (IResource) element;
			}
			else if (element instanceof IAdaptable) {
				resource = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
			}
			if (resource != null) {
				int i = elementList.indexOf(resource);
				if (i >= 0) {
					javaElements[i] = null;
				}
			}
		}
		for (int i = 0; i < javaElements.length; i++) {
			Object element = javaElements[i];
			if (element instanceof IJavaScriptElement) {
				IJavaScriptElement cElement = (IJavaScriptElement) element;
				IResource resource = cElement.getResource();
				if (resource != null) {
					proposedChildren.remove(resource);
				}
				proposedChildren.add(element);
			}
			else if (element != null) {
				proposedChildren.add(element);
			}
		}
	}

	public Object getPipelinedParent(Object object, Object suggestedParent) {
		if (object instanceof IJavaScriptElement && ((IJavaScriptElement) object).getElementType() == IJavaScriptElement.JAVASCRIPT_UNIT) {
			try {
				IResource underlyingResource = ((IJavaScriptUnit) object).getUnderlyingResource();
				if (underlyingResource != null && underlyingResource.getType() < IResource.PROJECT) {
					return underlyingResource.getParent();
				}
			}
			catch (JavaScriptModelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return suggestedParent;
	}

	public PipelinedShapeModification interceptAdd(PipelinedShapeModification addModification) {

		Object parent = addModification.getParent();

		if (parent instanceof IClosureProject) {
			addModification.setParent(((IClosureProject) parent).getProject());
		}

		if (parent instanceof IWorkspaceRoot) {
			deconvertJavaProjects(addModification);
		}

		convertToJavaElements(addModification);
		return addModification;
	}

	public PipelinedShapeModification interceptRemove(PipelinedShapeModification removeModification) {
		deconvertJavaProjects(removeModification);
		convertToJavaElements(removeModification.getChildren());
		return removeModification;
	}

	private void deconvertJavaProjects(PipelinedShapeModification modification) {
		Set convertedChildren = new LinkedHashSet();
		for (Iterator iterator = modification.getChildren().iterator(); iterator.hasNext();) {
			Object added = iterator.next();
			if (added instanceof IClosureProject) {
				iterator.remove();
				convertedChildren.add(((IClosureProject) added).getProject());
			}
		}
		modification.getChildren().addAll(convertedChildren);
	}

	/**
	 * Converts the shape modification to use Java elements.
	 * 
	 * 
	 * @param modification
	 *            the shape modification to convert
	 * @return returns true if the conversion took place
	 */
	private boolean convertToJavaElements(PipelinedShapeModification modification) {
		Object parent = modification.getParent();
		// As of 3.3, we no longer re-parent additions to IProject.
		if (parent instanceof IContainer) {
			IJavaScriptElement element = ClosureCore.create((IContainer) parent);
			if (element != null && element.exists()) {
				// we don't convert the root
				if (!(element instanceof IJavaScriptModel) && !(element instanceof IClosureProject))
					modification.setParent(element);
				return convertToJavaElements(modification.getChildren());

			}
		}
		return false;
	}


	/**
	 * Converts the shape modification to use Java elements.
	 * 
	 * 
	 * @param currentChildren
	 *            The set of current children that would be contributed or
	 *            refreshed in the viewer.
	 * @return returns true if the conversion took place
	 */
	private boolean convertToJavaElements(Set currentChildren) {

		LinkedHashSet convertedChildren = new LinkedHashSet();
		IJavaScriptElement newChild;
		for (Iterator childrenItr = currentChildren.iterator(); childrenItr.hasNext();) {
			Object child = childrenItr.next();
			// only convert IFolders and IFiles
			if (child instanceof IFolder || child instanceof IFile) {
				if ((newChild = ClosureCore.create((IResource) child)) != null && newChild.exists()) {
					childrenItr.remove();
					convertedChildren.add(newChild);
				}
			}
			else if (child instanceof IClosureProject) {
				childrenItr.remove();
				convertedChildren.add(((IClosureProject) child).getProject());
			}
		}
		if (!convertedChildren.isEmpty()) {
			currentChildren.addAll(convertedChildren);
			return true;
		}
		return false;
	}

	public boolean interceptRefresh(PipelinedViewerUpdate refreshSynchronization) {
		// return false;
		return convertToJavaElements(refreshSynchronization.getRefreshTargets());

	}

	public boolean interceptUpdate(PipelinedViewerUpdate updateSynchronization) {
		// return false;
		return convertToJavaElements(updateSynchronization.getRefreshTargets());
	}

	protected void postAdd(final Object parent, final Object element, Collection runnables) {
		if (parent instanceof IJavaScriptModel)
			super.postAdd(((IJavaScriptModel) parent).getWorkspace(), element, runnables);
		else if (parent instanceof IClosureProject)
			super.postAdd(((IClosureProject) parent).getProject(), element, runnables);
		else
			super.postAdd(parent, element, runnables);
	}


	protected void postRefresh(final List toRefresh, final boolean updateLabels, Collection runnables) {
		for (Iterator iter = toRefresh.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (element instanceof IJavaScriptModel) {
				iter.remove();
				toRefresh.add(element.equals(getViewerInput()) ? fRealInput : element);
				super.postRefresh(toRefresh, updateLabels, runnables);
				return;
			}
		}
		super.postRefresh(toRefresh, updateLabels, runnables);
	}

}
