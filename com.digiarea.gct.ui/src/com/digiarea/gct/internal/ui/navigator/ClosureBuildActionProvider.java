package com.digiarea.gct.internal.ui.navigator;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.actions.ActionContext;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonViewerWorkbenchSite;

import com.digiarea.gct.ui.actions.ClosureBuildActionGroup;

public class ClosureBuildActionProvider extends CommonActionProvider {

	private ClosureBuildActionGroup fClosureBuildGroup;
	private boolean fInViewPart = false;


	public ClosureBuildActionProvider() {
	}

	public void fillActionBars(IActionBars actionBars) {
		if (fInViewPart) {
			fClosureBuildGroup.fillActionBars(actionBars);
		}
	}

	public void fillContextMenu(IMenuManager menu) {
		if (fInViewPart) {
			fClosureBuildGroup.fillContextMenu(menu);
		}
	}

	public void init(ICommonActionExtensionSite site) {

		ICommonViewerWorkbenchSite workbenchSite = null;
		if (site.getViewSite() instanceof ICommonViewerWorkbenchSite)
			workbenchSite = (ICommonViewerWorkbenchSite) site.getViewSite();

		if (workbenchSite != null) {
			if (workbenchSite.getPart() != null && workbenchSite.getPart() instanceof IViewPart) {
				IViewPart viewPart = (IViewPart) workbenchSite.getPart();
				fClosureBuildGroup = new ClosureBuildActionGroup(viewPart);
				fInViewPart = true;
			}
		}
	}

	public void setContext(ActionContext context) {
		super.setContext(context);
		if (fInViewPart) {
			fClosureBuildGroup.setContext(context);
		}
	}

}
