/*******************************************************************************
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.wizards.buildpaths;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IIncludePathEntry;
import com.digiarea.gct.core.IJsGlobalScopeContainer;

public class CPUserLibraryElement implements ICPUserLibraryElement {

	private class UpdatedJsGlobalScopeContainer implements IJsGlobalScopeContainer {

		private boolean isClosure;

		public UpdatedJsGlobalScopeContainer(boolean isClosure) {
			this.isClosure = isClosure;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.digiarea.gct.core.IJsGlobalScopeContainer#getClasspathEntries()
		 */
		/**
		 * @deprecated Use {@link #getIncludepathEntries()} instead
		 */
		public IIncludePathEntry[] getClasspathEntries() {
			return getIncludepathEntries();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.digiarea.gct.core.IJsGlobalScopeContainer#getClasspathEntries()
		 */
		public IIncludePathEntry[] getIncludepathEntries() {
			CPListElement[] children = getChildren();
			IIncludePathEntry[] entries = new IIncludePathEntry[children.length];
			for (int i = 0; i < entries.length; i++) {
				entries[i] = children[i].getClasspathEntry();
			}
			return entries;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.digiarea.gct.core.IJsGlobalScopeContainer#getDescription()
		 */
		public String getDescription() {
			return getName();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.digiarea.gct.core.IJsGlobalScopeContainer#getKind()
		 */
		public int getKind() {
			if (isClosure) {
				return K_CLOSURE;
			}
			return K_APPLICATION;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.digiarea.gct.core.IJsGlobalScopeContainer#getPath()
		 */
		public IPath getPath() {
			return CPUserLibraryElement.this.getPath();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.digiarea.gct.core.IJsGlobalScopeContainer#resolvedLibraryImport
		 * (java.lang.String)
		 */
		public String[] resolvedLibraryImport(String a) {
			// TODO Auto-generated method stub
			return null;
		}
	}


	private String fName;
	private List fChildren;

	private boolean fIsClosure;

	public CPUserLibraryElement(String name, IJsGlobalScopeContainer container, IClosureProject project) {
		fName = name;
		fChildren = new ArrayList();
		if (container != null) {
			IIncludePathEntry[] entries = container.getIncludepathEntries();
			CPListElement[] res = new CPListElement[entries.length];
			for (int i = 0; i < res.length; i++) {
				IIncludePathEntry curr = entries[i];
				CPListElement elem = CPListElement.createFromExisting(this, curr, project);
				fChildren.add(elem);
			}
			fIsClosure = container.getKind() == IJsGlobalScopeContainer.K_CLOSURE;
		}
	}

	public CPUserLibraryElement(String name, CPListElement[] children, boolean isClosure) {
		fName = name;
		fChildren = new ArrayList();
		if (children != null) {
			for (int i = 0; i < children.length; i++) {
				fChildren.add(children[i]);
			}
		}
		fIsClosure = isClosure;
	}

	/**
	 * @return the fIsClosure
	 */
	public boolean isClosure() {
		return fIsClosure;
	}

	public CPListElement[] getChildren() {
		return (CPListElement[]) fChildren.toArray(new CPListElement[fChildren.size()]);
	}

	public String getName() {
		return fName;
	}

	public IPath getPath() {
		return new Path(ClosureCore.USER_LIBRARY_CONTAINER_ID).append(fName);
	}

	public void add(CPListElement element) {
		if (!fChildren.contains(element)) {
			fChildren.add(element);
		}
	}

	public void remove(CPListElement element) {
		fChildren.remove(element);
	}

	public void replace(CPListElement existingElement, CPListElement element) {
		if (fChildren.contains(element)) {
			fChildren.remove(existingElement);
		}
		else {
			int index = fChildren.indexOf(existingElement);
			if (index != -1) {
				fChildren.set(index, element);
			}
			else {
				fChildren.add(element);
			}
			element.setAttributesFromExisting(existingElement);
		}
	}

	public IJsGlobalScopeContainer getUpdatedContainer(boolean isClosure) {
		return new UpdatedJsGlobalScopeContainer(isClosure);
	}

	public boolean hasChanges(IJsGlobalScopeContainer oldContainer) {
		if (oldContainer == null) {
			return true;
		}
		IIncludePathEntry[] oldEntries = oldContainer.getIncludepathEntries();
		if (fChildren.size() != oldEntries.length) {
			return true;
		}
		for (int i = 0; i < oldEntries.length; i++) {
			CPListElement child = (CPListElement) fChildren.get(i);
			if (!child.getClasspathEntry().equals(oldEntries[i])) {
				return true;
			}
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.digiarea.gct.internal.ui.wizards.buildpaths.ICPUserLibraryElement#isDefault()
	 */
	public boolean isDefault() {
		return false;
	}


}
