/*******************************************************************************
 * Licensed Materials - Property of IBM
 * � Copyright IBM Corporation 2014. All Rights Reserved.
 * U.S. Government Users Restricted Rights - Use, duplication or disclosure
 * restricted by GSA ADP Schedule Contract with IBM Corp. 
 *******************************************************************************/

package com.digiarea.gct.internal.ui.wizards.buildpaths;

/**
 * @author daginno
 *
 */
public interface ICPUserLibraryElement {
	
	public String getName();
	
	public boolean isDefault();

}
