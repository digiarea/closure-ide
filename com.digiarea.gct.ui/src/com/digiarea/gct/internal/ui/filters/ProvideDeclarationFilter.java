/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.filters;


import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IProvideContainer;
import com.digiarea.gct.core.IProvideDeclaration;


/**
 * Filters import declarations
 */
public class ProvideDeclarationFilter extends ViewerFilter {

	/*
	 * @see ViewerFilter
	 */
	public boolean select(Viewer viewer, Object parent, Object element) {
		if (element instanceof IProvideDeclaration || element instanceof IProvideContainer) {
			return false;
		}
		else if (element instanceof IAdaptable) {
			IJavaScriptElement javaScriptElement = (IJavaScriptElement) ((IAdaptable) element).getAdapter(IJavaScriptElement.class);
			if (javaScriptElement != null && (javaScriptElement.getElementType() == IJavaScriptElement.PROVIDE_CONTAINER || javaScriptElement.getElementType() == IJavaScriptElement.PROVIDE_DECLARATION)) {
				return false;
			}
		}
		return true;
	}
}
