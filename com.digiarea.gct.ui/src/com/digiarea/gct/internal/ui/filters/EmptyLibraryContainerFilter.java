package com.digiarea.gct.internal.ui.filters;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import com.digiarea.gct.internal.ui.packageview.PackageFragmentRootContainer;
import com.digiarea.gct.internal.ui.viewsupport.ProblemTreeViewer;

/**
 * The library container filter is a filter used to determine whether library
 * containers are shown that are empty or have all children filtered out by
 * other filters. The filter is only applicable on a {@link ProblemTreeViewer}
 */
public class EmptyLibraryContainerFilter extends ViewerFilter {

	/*
	 * (non-Javadoc) Method declared on ViewerFilter.
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof PackageFragmentRootContainer && viewer instanceof ProblemTreeViewer) {
			return ((ProblemTreeViewer) viewer).hasFilteredChildren(element);
		}
		return true;
	}
}
