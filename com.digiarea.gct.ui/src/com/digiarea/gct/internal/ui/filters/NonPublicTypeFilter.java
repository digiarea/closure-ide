/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.filters;


import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import com.digiarea.gct.core.Flags;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IType;
import com.digiarea.gct.core.JavaScriptModelException;


/**
 * Filters non-public types
 */
public class NonPublicTypeFilter extends ViewerFilter {

	/*
	 * @see ViewerFilter
	 */
	public boolean select(Viewer viewer, Object parent, Object element) {
		Object type = null;
		if(element instanceof IType){
			type = element;
		}else if(element instanceof IAdaptable){
			element = ((IAdaptable) element).getAdapter(IJavaScriptElement.class);
		}
		if (element instanceof IType) {
			IType type1= (IType)element;
			try {
				return Flags.isPublic(type1.getFlags());
			} catch (JavaScriptModelException ex) {
				return true;
			}
		}
		return true;
	}
}
