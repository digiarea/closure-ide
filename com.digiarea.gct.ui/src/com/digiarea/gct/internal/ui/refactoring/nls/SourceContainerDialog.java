/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.refactoring.nls;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IPackageFragmentRoot;
import com.digiarea.gct.core.JavaScriptModelException;
import com.digiarea.gct.internal.ui.wizards.NewWizardMessages;
import com.digiarea.gct.internal.ui.wizards.TypedElementSelectionValidator;
import com.digiarea.gct.internal.ui.wizards.TypedViewerFilter;
import com.digiarea.gct.ui.JavaScriptElementComparator;
import com.digiarea.gct.ui.JavaScriptElementLabelProvider;
import com.digiarea.gct.ui.StandardJavaScriptElementContentProvider;

public class SourceContainerDialog extends ElementTreeSelectionDialog {

	private class PackageAndProjectSelectionValidator extends TypedElementSelectionValidator {

		public PackageAndProjectSelectionValidator() {
			super(new Class[]{IPackageFragmentRoot.class},false);
		}

		public boolean isSelectedValid(Object element) {
			try {
				if (element instanceof IClosureProject) {
					IClosureProject jproject= (IClosureProject) element;
					IPath path= jproject.getProject().getFullPath();
					return (jproject.findPackageFragmentRoot(path) != null);
				} else
					if (element instanceof IPackageFragmentRoot) {
						return (((IPackageFragmentRoot) element).getKind() == IPackageFragmentRoot.K_SOURCE);
					}
				return true;
			} catch (JavaScriptModelException e) {
				// fall through returning false
			}
			return false;
		}
	}
	
	/**
	 * A TypedViewerFilter that accepts only PackageFragments and JavaProjects.
	 * PackageFragments are only accepted if they are of the kind K_SOURCE.
	 */
	private class JavaTypedViewerFilter extends TypedViewerFilter {

		public JavaTypedViewerFilter() {
			super(new Class[]{IPackageFragmentRoot.class, IClosureProject.class});
		}

		public boolean select(Viewer viewer, Object parent, Object element) {
			if (element instanceof IPackageFragmentRoot) {
				IPackageFragmentRoot fragmentRoot= (IPackageFragmentRoot)element;
				try {
					return (fragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE);
				} catch (JavaScriptModelException e) {
					return false;
				}
			}
			return super.select(viewer, parent, element);
		}
	}

	private SourceContainerDialog(Shell shell) {
		super(shell,new JavaScriptElementLabelProvider(JavaScriptElementLabelProvider.SHOW_DEFAULT),new StandardJavaScriptElementContentProvider());
		setValidator(new PackageAndProjectSelectionValidator());
		setComparator(new JavaScriptElementComparator());
		setTitle(NewWizardMessages.NewContainerWizardPage_ChooseSourceContainerDialog_title); 
		setMessage(NewWizardMessages.NewContainerWizardPage_ChooseSourceContainerDialog_description); 
		addFilter(new JavaTypedViewerFilter());
	}

	public static IPackageFragmentRoot getSourceContainer(Shell shell, IWorkspaceRoot workspaceRoot, IJavaScriptElement initElement) {
		SourceContainerDialog dialog= new SourceContainerDialog(shell);
		dialog.setInput(ClosureCore.create(workspaceRoot));
		dialog.setInitialSelection(initElement);

		if (dialog.open() == Window.OK) {
			Object element= dialog.getFirstResult();
			if (element instanceof IClosureProject) {
				IClosureProject jproject= (IClosureProject) element;
				return jproject.getPackageFragmentRoot(jproject.getProject());
			} else
				if (element instanceof IPackageFragmentRoot) {
					return (IPackageFragmentRoot) element;
				}
			return null;
		}
		return null;
	}
}
