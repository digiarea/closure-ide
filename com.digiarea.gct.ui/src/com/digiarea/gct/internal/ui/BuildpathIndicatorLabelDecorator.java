/*******************************************************************************
 * Copyright (c) 2009, 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;

import com.digiarea.gct.core.ClosureCore;
import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IJavaScriptElementDelta;
import com.digiarea.gct.core.IPackageFragmentRoot;
import com.digiarea.gct.core.JavaScriptModelException;

public class BuildpathIndicatorLabelDecorator extends AbstractJavaElementLabelDecorator {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decorate(Object element, IDecoration decoration) {
		ImageDescriptor overlay = getOverlay(element);
		if (overlay != null) {
			decoration.addOverlay(overlay, IDecoration.BOTTOM_LEFT);
		}
	}

	private ImageDescriptor getOverlay(Object element) {
		if (element instanceof IResource) {
			IResource resource = (IResource) element;
			IProject project = resource.getProject();
			if (project != null) {
				IClosureProject javaProject = ClosureCore.create(project);
				if (javaProject != null) {
					if (javaProject.isOnIncludepath(resource)) {
						IJavaScriptElement javaElement = ClosureCore.create(resource, javaProject);
						try {
							if (javaElement instanceof IPackageFragmentRoot && ((IPackageFragmentRoot) javaElement).getKind() != IPackageFragmentRoot.K_SOURCE) {
								return JavaPluginImages.DESC_OVR_LIBRARY;
							}
						}
						catch (JavaScriptModelException e) {
							return null;
						}
					}
				}
			}
		}
		return null;
	}

	@Override
	protected void processDelta(IJavaScriptElementDelta delta, List<IJavaScriptElement> result) {
		IJavaScriptElement elem = delta.getElement();

		boolean isChanged = delta.getKind() == IJavaScriptElementDelta.CHANGED;
		boolean isRemoved = delta.getKind() == IJavaScriptElementDelta.REMOVED;
		int flags = delta.getFlags();

		switch (elem.getElementType()) {
			case IJavaScriptElement.JAVASCRIPT_MODEL :
				processChildrenDelta(delta, result);
				return;
			case IJavaScriptElement.JAVASCRIPT_PROJECT :
				if (isRemoved || (isChanged && (flags & IJavaScriptElementDelta.F_CLOSED) != 0)) {
					return;
				}
				processChildrenDelta(delta, result);
				return;
			case IJavaScriptElement.PACKAGE_FRAGMENT_ROOT :
				if (isRemoved) {
					return;
				}
				try {
					if ((((flags & IJavaScriptElementDelta.F_REMOVED_FROM_CLASSPATH) != 0) || ((flags & IJavaScriptElementDelta.F_ADDED_TO_CLASSPATH) != 0)) && ((IPackageFragmentRoot) elem).getKind() != IPackageFragmentRoot.K_SOURCE) {
						result.add(elem);
					}
				}
				catch (JavaScriptModelException e) {
					// don't update
				}
				return;
			default :
				return;
		}
	}

}
