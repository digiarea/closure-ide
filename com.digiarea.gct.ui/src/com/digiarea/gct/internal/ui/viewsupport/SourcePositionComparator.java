/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.internal.ui.viewsupport;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import com.digiarea.gct.core.Flags;
import com.digiarea.gct.core.IClassFile;
import com.digiarea.gct.core.IJavaScriptElement;
import com.digiarea.gct.core.IJavaScriptUnit;
import com.digiarea.gct.core.IMember;
import com.digiarea.gct.core.ISourceRange;
import com.digiarea.gct.core.ISourceReference;
import com.digiarea.gct.core.IType;
import com.digiarea.gct.core.JavaScriptModelException;


/**
 *  Viewer sorter which sorts the Java elements like
 *  they appear in the source.
 * 
 * 
 */
public class SourcePositionComparator extends ViewerComparator {

	/*
	 * @see org.eclipse.jface.viewers.ViewerSorter#compare(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public int compare(Viewer viewer, Object el1, Object el2) {

		ISourceReference element1 = null;
		ISourceReference element2 = null;

		if (el1 instanceof ISourceReference) {
			element1 = (ISourceReference) el1;
		}
		else if (el1 instanceof IAdaptable) {
			element1 = (ISourceReference) ((IAdaptable) el1).getAdapter(ISourceReference.class);
		}
		
		if(el2 instanceof ISourceReference){
			element2 = (ISourceReference)el2;
		}else
		if(el2 instanceof IAdaptable){
			element2 = (ISourceReference) ((IAdaptable)el2).getAdapter(ISourceReference.class);
		}
		
		if(element1  == null|| element2 == null){
			return 0;
		}
		
		IJavaScriptElement parent1= ((IJavaScriptElement)element1).getParent();
		IJavaScriptElement parent2= ((IJavaScriptElement)element2).getParent();
		if (parent1 == null || !parent1.equals(parent2)) {
				IType t1= getOutermostDeclaringType(element1);
				if (t1 == null)
					return 0;
				
				IType t2= getOutermostDeclaringType(element2);
				try {
					if (!t1.equals(t2)) {
						if (t2 == null)
							return 0;

						if (Flags.isPublic(t1.getFlags()) && Flags.isPublic(t2.getFlags()))
							return 0;

						if (!t1.getPackageFragment().equals(t2.getPackageFragment()))
							return 0;

						IJavaScriptUnit cu1= (IJavaScriptUnit)((IJavaScriptElement)element2).getAncestor(IJavaScriptElement.JAVASCRIPT_UNIT);
						if (cu1 != null) {
							if (!cu1.equals(((IJavaScriptElement)element2).getAncestor(IJavaScriptElement.JAVASCRIPT_UNIT)))
								return 0;
						} else {
							IClassFile cf1= (IClassFile)((IJavaScriptElement)element2).getAncestor(IJavaScriptElement.CLASS_FILE);
							if (cf1 == null)
								return 0;
							IClassFile cf2= (IClassFile)((IJavaScriptElement)element2).getAncestor(IJavaScriptElement.CLASS_FILE);
							String source1= cf1.getSource();
							if (source1 != null && !source1.equals(cf2.getSource()))
								return 0;
						}
					}
				} catch (JavaScriptModelException e3) {
					return 0;
				}
		}
		
		try {
			ISourceRange sr1= element1.getSourceRange();
			ISourceRange sr2=element2.getSourceRange();
			if (sr1 == null || sr2 == null)
				return 0;
			
			return sr1.getOffset() - sr2.getOffset();
			
		} catch (JavaScriptModelException e) {
			return 0;
		}
	}

	private IType getOutermostDeclaringType(Object element) {
		if (!(element instanceof IMember))
			return null;
		
		IType declaringType;
		if (element instanceof IType)
			declaringType= (IType)element;
		else {
			declaringType= ((IMember)element).getDeclaringType();
			if (declaringType == null)
				return null;
		}
		
		IType declaringTypeDeclaringType= declaringType.getDeclaringType();
		while (declaringTypeDeclaringType != null) {
			declaringType= declaringTypeDeclaringType;
			declaringTypeDeclaringType= declaringType.getDeclaringType();
		}
		return declaringType;
	}
}
