/*******************************************************************************
 * Copyright (c) 2007, 2014 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.digiarea.gct.web.ui.internal.contentassist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.ui.contentassist.CompletionProposalInvocationContext;
import org.eclipse.wst.sse.ui.contentassist.ICompletionProposalComputer;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.wst.xml.ui.internal.contentassist.AbstractContentAssistProcessor;

import com.digiarea.gct.core.CompletionProposal;
import com.digiarea.gct.web.core.javascript.IJsTranslation;
import com.digiarea.gct.web.core.javascript.JsTranslationAdapter;
/**
*

* Provisional API: This class/interface is part of an interim API that is still under development and expected to
* change significantly before reaching stability. It is being made available at this early stage to solicit feedback
* from pioneering adopters on the understanding that any code that uses this API will almost certainly be broken
* (repeatedly) as the API evolves.
*/
public class GCTContentAssistant extends AbstractContentAssistProcessor implements ICompletionProposalComputer {
	private GCTContentAssistantProcessor fContentAssistProcessor;
	private GCTTemplateAssistProcessor fTemplateAssistProcessor;
	private GCTHtmlCompletionProcessor fHhtmlcomp;
	
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int documentPosition) {
		Vector proposals = new Vector();
		ICompletionProposal[] completionProposals;
		ICompletionProposal endScript = getHtmlContentAssistProcessor().getEndScriptProposal(viewer, documentPosition);
		if(endScript!=null) {
			return new ICompletionProposal[] {endScript};
			//proposals.add(endScript);
		}
		GCTProposalCollector theCollector = getProposalCollector(viewer, documentPosition);
		/* add end script tag if needed */

		/* --------- Content Assistant --------- */
		if(theCollector==null) return new ICompletionProposal[0];
		
		getContentAssistProcessor().setProposalCollector(theCollector);
		completionProposals = getContentAssistProcessor().computeCompletionProposals(viewer, documentPosition);
		proposals.addAll(Arrays.asList(completionProposals));
		/* HTML Proposals */
		completionProposals = getHtmlContentAssistProcessor().computeCompletionProposals(viewer, documentPosition);
		proposals.addAll(Arrays.asList(completionProposals));
		/* --------- template completions --------- */
		getTemplateCompletionProcessor().setProposalCollector(theCollector);
		completionProposals = getTemplateCompletionProcessor().computeCompletionProposals(viewer, documentPosition);
		proposals.addAll(Arrays.asList(completionProposals));
		return (ICompletionProposal[]) proposals.toArray(new ICompletionProposal[0]);
	}
	
	private GCTHtmlCompletionProcessor getHtmlContentAssistProcessor() {
		if (fHhtmlcomp == null) {
			fHhtmlcomp = new GCTHtmlCompletionProcessor();
		}
		return fHhtmlcomp;
	}
	
	private GCTContentAssistantProcessor getContentAssistProcessor() {
		if (fContentAssistProcessor == null) {
			fContentAssistProcessor = new GCTContentAssistantProcessor();
		}
		return fContentAssistProcessor;
	}
	private IJsTranslation getJSPTranslation(ITextViewer viewer, int offset) {
		IDOMModel xmlModel = null;
		try {
			xmlModel = (IDOMModel) StructuredModelManager.getModelManager().getExistingModelForRead(viewer.getDocument());
			IDOMDocument xmlDoc = xmlModel.getDocument();

			JsTranslationAdapter translationAdapter = (JsTranslationAdapter) xmlDoc.getAdapterFor(IJsTranslation.class);
			
			if (translationAdapter != null) {
				return translationAdapter.getJsTranslation(true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (xmlModel != null) {
				xmlModel.releaseFromRead();
			}
		}
		return null;
	}
	
	protected GCTProposalCollector getProposalCollector(ITextViewer viewer, int offset) {
		IJsTranslation tran = getJSPTranslation(viewer, offset);
		if(tran==null) return null;
		return new GCTProposalCollector(tran);
	}
	
	private GCTTemplateAssistProcessor getTemplateCompletionProcessor() {
		if (fTemplateAssistProcessor == null) {
			fTemplateAssistProcessor = new GCTTemplateAssistProcessor();
		}
		return fTemplateAssistProcessor;
	}

	public void sessionStarted() {
		// TODO Auto-generated method stub
		
	}

	public List computeCompletionProposals(
			CompletionProposalInvocationContext context,
			IProgressMonitor monitor) {
		Vector proposals = new Vector();
		ICompletionProposal[] completionProposals;
		ICompletionProposal endScript = getHtmlContentAssistProcessor().getEndScriptProposal(context.getViewer(), context.getInvocationOffset());
		if(endScript!=null) {
			return new ArrayList(0);
			//proposals.add(endScript);
		}
		GCTProposalCollector theCollector = getProposalCollector(context.getViewer(), context.getInvocationOffset());
		if(theCollector==null) return new ArrayList(0);
		
		/* add end script tag if needed */
		
		theCollector.setAllowsRequiredProposals(CompletionProposal.CONSTRUCTOR_INVOCATION, CompletionProposal.TYPE_REF, true);

		/* --------- Content Assistant --------- */
		
		getContentAssistProcessor().setProposalCollector(theCollector);
		completionProposals = getContentAssistProcessor().computeCompletionProposals(context.getViewer(), context.getInvocationOffset());
		proposals.addAll(Arrays.asList(completionProposals));
		/* HTML Proposals */
		completionProposals = getHtmlContentAssistProcessor().computeCompletionProposals(context.getViewer(), context.getInvocationOffset());
		proposals.addAll(Arrays.asList(completionProposals));
		/* --------- template completions --------- */
		getTemplateCompletionProcessor().setProposalCollector(theCollector);
		completionProposals = getTemplateCompletionProcessor().computeCompletionProposals(context.getViewer(), context.getInvocationOffset());
		proposals.addAll(Arrays.asList(completionProposals));
		return proposals;
	}

	public List computeContextInformation(
			CompletionProposalInvocationContext context,
			IProgressMonitor monitor) {
		return Arrays.asList(computeContextInformation(context.getViewer(), context.getInvocationOffset()));
	}

	public void sessionEnded() {
		fContentAssistProcessor = null;
		fTemplateAssistProcessor = null;
		fHhtmlcomp = null;
	}
}
