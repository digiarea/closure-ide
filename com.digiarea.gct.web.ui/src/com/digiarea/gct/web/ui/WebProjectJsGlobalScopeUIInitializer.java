/*******************************************************************************
 * Copyright (c) 2007, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package com.digiarea.gct.web.ui;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.resource.ImageDescriptor;

import com.digiarea.gct.core.IClosureProject;
import com.digiarea.gct.internal.ui.IJsGlobalScopeContainerInitializerExtension;
/**
*

* Provisional API: This class/interface is part of an interim API that is still under development and expected to
* change significantly before reaching stability. It is being made available at this early stage to solicit feedback
* from pioneering adopters on the understanding that any code that uses this API will almost certainly be broken
* (repeatedly) as the API evolves.
*
 * @author childsb
 *
 */
public class WebProjectJsGlobalScopeUIInitializer implements IJsGlobalScopeContainerInitializerExtension{
	/* (non-Javadoc)
	 * @see com.digiarea.gct.internal.ui.IJsGlobalScopeContainerInitialzerExtension#getImage(org.eclipse.core.runtime.IPath, java.lang.String, com.digiarea.gct.core.IClosureProject)
	 */
	public ImageDescriptor getImage(IPath containerPath, String element, IClosureProject project) {
		return ImageDescriptor.createFromFile(this.getClass(),"web1.JPG"); //$NON-NLS-1$
	}
}
